{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Streaming.Learn where

import Control.Monad.Managed
import qualified Data.Text.IO as T
import Protolude
import Streaming
import Streaming.Internal
import Streaming.Prelude (next)
import System.IO

yield :: a -> Stream (Of a) m r -> Stream (Of a) m r
yield x = Step . (x :>)

each :: [a] -> Stream (Of a) m ()
each = foldr yield $ Return ()

steps :: Monad m => Stream (Of a) m b -> m ([a], b)
steps s = do
  x <- next s
  case x of
    Left r -> pure ([], r)
    Right (x, s') -> first (x :) <$> steps s'

-- | Output from parsing errors.
streamFile :: MonadManaged m => FilePath -> Stream (Of Text) m Int
streamFile path = Effect do
  h <- managed do
    bracket
      do openBinaryFile path ReadMode
      do hClose
  let loop count = Effect $ liftIO do
        t <- T.hGetChunk h
        eof <- hIsEOF h
        pure $
          yield t $
            if eof
              then Effect $ liftIO do
                hClose h
                pure $ Return count
              else loop (succ count)
  pure $ loop 0

-- abstracting generators
streamLoop
  :: Functor m
  => (k -> f (Stream f m r) -> (k -> Stream f m r -> Stream g m r') -> Stream g m r')
  -> (k -> r -> r')
  -> k
  -> Stream f m r
  -> Stream g m r'
streamLoop step return = go
  where
    go state (Return r) = Return $ return state r
    go state (Effect m) = Effect $ go state <$> m
    go state (Step f) = step state f go

