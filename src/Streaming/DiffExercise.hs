{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

-- helper to synchronize a database slave table to from a database master table
-- by streaming the operation to be performed on the slave, computed while while streaming both
-- the 't' column is the streaming key so it's intended (non strictly) monotonically growing
-- so we can dump all  operations regarding a specific 't' if we lockstep the 2 streams on it

module Streaming.Diffs where

-- import qualified Data.Map.Monoidal.Strict as M -- monoidal-containers to harness monoid
import qualified Data.Map.Strict as M
import Protolude
import Streaming
import qualified Streaming.Prelude as S
import Test.Hspec
import Test.QuickCheck

--------------- next 3 stream operations were useful for my solution, decide if to implement them -------------

-- helps to implement integrate
breaker
  :: (Eq t, Monad m)
  => (a -> t)
  -> Stream (Of a) m r
  -> Stream (Of a) m (Either r (t, Stream (Of a) m r))
breaker = notImplemented

-- useful to prepare the streams for merging
integrate
  :: (Eq t, Monad m, Monoid b)
  => Stream (Of (t, b)) m r
  -> Stream (Of (t, b)) m r
integrate = notImplemented

-- merge 2 streams where 't' is strictly monotonic on both, resolving matches on 't' with 'b' semigroup
-- useful for streamAssocDiffs
mergeSemigroup
  :: (Ord t, Monad m, Semigroup b)
  => Stream (Of (t, b)) m r
  -> Stream (Of (t, b)) m r
  -> Stream (Of (t, b)) m (r, r)
mergeSemigroup = notImplemented

------------------ this is to be implemented to pass the tests ---------------------------

-- report what to do
-- this is not the semigroup 'v' you need to be the map (k -> v) a monoid under union
-- if you want to use mergeSemigroup you need a semigroup that can be viewed as Update as final step
data Update v = Update v v | Delete v | New v deriving (Show, Eq)

-- stream out the differences fot the 2 streams
-- for same 't' and same 'k' and different 'v' we want Update v1 v2
-- for same 't' and same 'k' and same 'v' we want no signal
-- for same 't' and 'k' only in first stream we want Delete v1
-- for same 't' and 'k' only in second stream we want New v2

streamAssocsDiffs
  :: (Monad m, Ord t, Ord k, Eq v)
  => Stream (Of (t, (k, v))) m r
  -> Stream (Of (t, (k, v))) m r
  -> Stream (Of (t, (k, Update v))) m (r, r)
streamAssocsDiffs = notImplemented

----------------- tests -----------------------------------------

-- this is a non-streaming reference imlementation based on Data.Map difference and intersection

referenceDiffs :: (Ord t, Ord k, Eq v) => [(t, (k, v))] -> [(t, (k, v))] -> [(t, (k, Update v))]
referenceDiffs s1 s2 =
  let m1 = M.fromList $ fmap (\(t, (k, v)) -> ((t, k), v)) s1
      m2 = M.fromList $ fmap (\(t, (k, v)) -> ((t, k), v)) s2
      delete = Delete <$> M.difference m1 m2
      new = New <$> M.difference m2 m1
      update = M.intersectionWith Update m1 m2
   in fmap (\((t, k), v) -> (t, (k, v))) $
        filter \case
          (_, Update v1 v2) -> v1 /= v2
          _ -> True
          $ M.assocs (delete <> update <> new)

-- I claim (O_O) swtitching the streams from the tables produce a reversed stream of operations where
reverseDiff :: Update v -> Update v
reverseDiff (Delete x) = New x
reverseDiff (New x) = Delete x
reverseDiff (Update x y) = Update y x

randomList :: Int -> Gen [(Int, (Int, Int))]
randomList r = do
  l :: Int <- choose (1, r)
  let next l' n m x
        | l' == l = pure []
        | otherwise = do
          cn <- elements [False, False, False, True] -- time speed
          cd <- choose (1, 2)
          let (n', m') = if cn then (n + cd, cd - 1) else (n, m + 1) -- don't repeat keys in the same time
          cm <- elements [False, False, True] -- key speed
          cd <- choose (1, 2)
          let (m'', x') = if cm then (m' + cd, cd - 1) else (m', x)
          cx <- elements [False, False, False, True] -- value change
          cd <- choose (1, 2)
          let x'' = if cx then x' + cd else x'
          ((n', (m'', x'')) :) <$> next (l' + 1) n' m'' x''
  next 0 0 0 0

-- renounce the streaming in the tests
runStreamDiffs :: (Ord t, Ord k, Eq v) => [(t, (k, v))] -> [(t, (k, v))] -> [(t, (k, Update v))]
runStreamDiffs s1 s2 =
  let ls :> _ = runIdentity $ S.toList $ streamAssocsDiffs (S.each s1) (S.each s2)
   in ls

main :: IO ()
main = hspec do
  describe "streamDiffs" do
    it "produces no diffs for same stream" $
      shouldBe
        do referenceDiffs [(1, (1, 1))] [(1, (1, 1))]
        do []
    it "produces deletes for left stream " $
      shouldBe
        do referenceDiffs [(1, (1, 1))] []
        do [(1, (1, Delete 1))]
    it "produces news for right stream " $
      shouldBe
        do referenceDiffs [] [(1, (1, 1))]
        do [(1, (1, New 1))]
    it "produces delete news for uncrossing streams " $
      shouldBe
        do referenceDiffs [(1, (1, 1))] [(2, (1, 1))]
        do [(1, (1, Delete 1)), (2, (1, New 1))]
    it "produces Updates for different crossing streams" $
      shouldBe
        do referenceDiffs [(1, (1, 1))] [(1, (1, 2))]
        do [(1, (1, Update 1 2))]
    it "produces no diffs for same stream" $
      shouldBe
        do runStreamDiffs [(1, (1, 1))] [(1, (1, 1))]
        do []
    it "produces deletes for left stream " $
      shouldBe
        do runStreamDiffs [(1, (1, 1))] []
        do [(1, (1, Delete 1))]
    it "produces news for right stream " $
      shouldBe
        do runStreamDiffs [] [(1, (1, 1))]
        do [(1, (1, New 1))]
    it "produces delete news for uncrossing streams " $
      shouldBe
        do runStreamDiffs [(1, (1, 1))] [(2, (1, 1))]
        do [(1, (1, Delete 1)), (2, (1, New 1))]
    it "produces Updates for different crossing streams" $
      shouldBe
        do runStreamDiffs [(1, (1, 1))] [(1, (1, 2))]
        do [(1, (1, Update 1 2))]
    it "match the pure implementation" $
      property $
        forAll ((,) <$> randomList 1000 <*> randomList 1000) $ \(l1, l2) -> shouldBe
          do referenceDiffs l1 l2
          do runStreamDiffs l1 l2
    it "switching list reverses diffs " $
      property $
        forAll ((,) <$> randomList 1000 <*> randomList 1000) $ \(l1, l2) -> shouldBe
          do runStreamDiffs l1 l2
          do fmap (fmap reverseDiff) <$> runStreamDiffs l2 l1
