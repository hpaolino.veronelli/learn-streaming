{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Streaming.Parser where

import Data.Attoparsec.Text
    ( parse, IResult(Done, Partial, Fail), Parser )
import qualified Data.Text as T
import Protolude ( Text, (&), identity )
import Streaming ( Of, Stream )
import Streaming.Internal (Stream (..))
import Streaming.Prelude (next)
import Text.Taggy (tag)
import Text.Taggy.Types ( Tag )
import Streaming.Learn ( yield )


type ParseStream m a r = Stream (Of Text) m r -> Stream (Of a) m (Stream (Of Text) m r)

-- type Restore m a r = Stream (Of Text) m r -> Stream (Of Text) m r

streamParser :: forall m a r. Monad m => Parser a -> ParseStream m a r
streamParser parser = start Nothing
  where
    start Nothing = loop (parse parser "") True identity -- start parser with no text
    start (Just rest) = loop (parse parser rest) False $ yield rest -- start parser with leftovers
    loop (Partial continue) isFresh rollback s = Effect do
      lr <- next s
      pure $ case lr of
        Right (t, s') ->
          loop (continue t) False (rollback . yield t) s' -- feed and remember
        Left r ->
          if isFresh
            then Return (Return r) -- good work
            else loop (continue "") False rollback s -- make it fail
    loop Fail {} _ rollback s = Return (rollback s)
    loop (Done rest x) _ _ s =
      yield x do
        s & start do
          if T.null rest
            then Nothing
            else Just rest

tags :: Monad m => ParseStream m Tag r
tags = streamParser (tag True)