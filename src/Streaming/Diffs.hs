{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Streaming.Diffs where

import qualified Data.Map.Monoidal.Strict as M
import qualified Data.Map.Strict as Mn
import Protolude
import Streaming
import Streaming.Internal
import qualified Streaming.Prelude as S
import Test.Hspec
import Test.QuickCheck

breaker
  :: (Eq t, Monad m)
  => (a -> t)
  -> Stream (Of a) m r
  -> Stream (Of a) m (Either r (t, Stream (Of a) m r))
breaker _ (Return r) = Return (Left r)
breaker tag s = Effect do
  x <- inspect s
  pure $ case x of
    Left r -> Return (Left r)
    Right (y :> s') -> go (tag y) $ S.cons y s'
  where
    go t' stream =
      Effect do
        x <- inspect stream
        pure case x of
          Left r -> Return $ Right (t', Return r)
          Right (y :> stream') ->
            let t = tag y
             in if t' == t
                  then Step $ y :> go t stream'
                  else Return $ Right (t', Step $ y :> stream')

integrate
  :: (Eq t, Monad m, Monoid b)
  => Stream (Of (t, b)) m r
  -> Stream (Of (t, b)) m r
integrate stream = Effect $ do
  b :> mr <- S.foldMap snd $ breaker fst stream
  pure $ case mr of
    Left r -> Return r
    Right (t, stream') -> Step $ (t, b) :> integrate stream'

mergeSemigroup :: (Ord t, Monad m, Semigroup b) => Stream (Of (t, b)) m r -> Stream (Of (t, b)) m r -> Stream (Of (t, b)) m (r, r)
mergeSemigroup (Return r) s = (r,) <$> s
mergeSemigroup s (Return r) = (,r) <$> s
mergeSemigroup (Effect m) s = Effect $ m <&> \l -> mergeSemigroup l s
mergeSemigroup s (Effect m) = Effect $ m <&> \r -> mergeSemigroup s r
mergeSemigroup ls@(Step ((lt, lb) :> lr)) rs@(Step ((rt, rb) :> rr))
  | lt < rt = S.yield (lt, lb) >> mergeSemigroup lr rs
  | lt > rt = S.yield (rt, rb) >> mergeSemigroup ls rr
  | otherwise = S.yield (lt, lb <> rb) >> mergeSemigroup lr rr

type LRB a = (Last a, Last a)

mkL :: a -> LRB a
mkL x = (Last $ Just x, mempty)

mkR :: a -> LRB a
mkR y = (mempty, Last $ Just y)

lrb2Update :: Eq v => (Last v, Last v) -> Maybe (Update v)
lrb2Update (Last (Just x), Last (Just y))
  | x == y = Nothing
  | otherwise = Just $ Update x y
lrb2Update (Last (Just x), _) = Just $ Delete x
lrb2Update (_, Last (Just y)) = Just $ New y

streamDiffs
  :: (Monad m, Functor f, Ord t, Monoid (f (LRB a)), Eq a)
  => Stream (Of (t, f a)) m r
  -> Stream (Of (t, f a)) m r
  -> Stream (Of (t, f (Maybe (Update a)))) m (r, r)
streamDiffs old new =
  S.map (fmap $ fmap lrb2Update) $
    mergeSemigroup (prepare mkL old) (prepare mkR new)
  where
    prepare f = integrate . S.map (fmap $ fmap f)

data Update v = Update v v | Delete v | New v deriving (Show, Eq)

streamAssocsDiffs
  :: (Monad m, Ord t, Ord k, Eq v)
  => Stream (Of (t, (k, v))) m r
  -> Stream (Of (t, (k, v))) m r
  -> Stream (Of (t, (k, Update v))) m (r, r)
streamAssocsDiffs s1 s2 =
  S.for
    do streamDiffs (S.map prepare s1) (S.map prepare s2)
    do \(t, q) -> S.catMaybes $ S.each $ (\(k, v) -> (t,) . (k,) <$> v) <$> M.assocs q
  where
    prepare (t, (k, v)) = (t, M.singleton k v)

----------------- test -----------------------------------------

pureDiffs :: [(Int, (Int, Int))] -> [(Int, (Int, Int))] -> [(Int, (Int, Update Int))]
pureDiffs s1 s2 =
  let m1 = Mn.fromList $ fmap (\(t, (k, v)) -> ((t, k), v)) s1
      m2 = Mn.fromList $ fmap (\(t, (k, v)) -> ((t, k), v)) s2
      delete = Delete <$> Mn.difference m1 m2
      new = New <$> Mn.difference m2 m1
      update = Mn.intersectionWith Update m1 m2
   in fmap (\((t, k), v) -> (t, (k, v))) $
        filter \case
          (_, Update v1 v2) -> v1 /= v2
          _ -> True
          $ Mn.assocs (delete <> update <> new)

randomList :: Int -> Gen [(Int, (Int, Int))]
randomList r = do
  l :: Int <- choose (1, r)
  let next l' n m x
        | l' == l = pure []
        | otherwise = do
          cn <- elements [False, False, False, True] -- time speed
          cd <- choose (1, 2)
          let (n', m') = if cn then (n + cd, cd - 1) else (n, m + 1)
          cm <- elements [False, False, True]
          cd <- choose (1, 2)
          let (m'', x') = if cm then (m' + cd, cd - 1) else (m', x)
          cx <- elements [False, False, False, True]
          cd <- choose (1, 2)
          let x'' = if cx then x' + cd else x'
          ((n', (m'', x'')) :) <$> next (l' + 1) n' m'' x''
  next 0 0 0 0

testDiffs :: [(Int, (Int, Int))] -> [(Int, (Int, Int))] -> [(Int, (Int, Update Int))]
testDiffs s1 s2 =
  let ls :> _ = runIdentity $ S.toList $ streamAssocsDiffs @Identity (S.each s1) (S.each s2)
   in ls

reverseDiff :: Update v -> Update v
reverseDiff (Delete x) = New x
reverseDiff (New x) = Delete x
reverseDiff (Update x y) = Update y x

main :: IO ()
main = hspec do
  describe "streamDiffs" do
    it "gets no diffs for same stream" $
      shouldBe
        do pureDiffs [(1, (1, 1))] [(1, (1, 1))]
        do []
    it "gets deletes for left stream " $
      shouldBe
        do pureDiffs [(1, (1, 1))] []
        do [(1, (1, Delete 1))]
    it "gets news for right stream " $
      shouldBe
        do pureDiffs [] [(1, (1, 1))]
        do [(1, (1, New 1))]
    it "gets delete news for uncrossing streams " $
      shouldBe
        do pureDiffs [(1, (1, 1))] [(2, (1, 1))]
        do [(1, (1, Delete 1)), (2, (1, New 1))]
    it "gets Updates for different crossing streams" $
      shouldBe
        do pureDiffs [(1, (1, 1))] [(1, (1, 2))]
        do [(1, (1, Update 1 2))]
    it "gets no diffs for same stream" $
      shouldBe
        do testDiffs [(1, (1, 1))] [(1, (1, 1))]
        do []
    it "gets deletes for left stream " $
      shouldBe
        do testDiffs [(1, (1, 1))] []
        do [(1, (1, Delete 1))]
    it "gets news for right stream " $
      shouldBe
        do testDiffs [] [(1, (1, 1))]
        do [(1, (1, New 1))]
    it "gets delete news for uncrossing streams " $
      shouldBe
        do testDiffs [(1, (1, 1))] [(2, (1, 1))]
        do [(1, (1, Delete 1)), (2, (1, New 1))]
    it "gets Updates for different crossing streams" $
      shouldBe
        do testDiffs [(1, (1, 1))] [(1, (1, 2))]
        do [(1, (1, Update 1 2))]
    it "match the pure implementation" $
      property $
        forAll ((,) <$> randomList 1000 <*> randomList 1000) $ \(l1, l2) -> shouldBe
          do pureDiffs l1 l2
          do testDiffs l1 l2
    it "switching list reverses siffs " $
      property $
        forAll ((,) <$> randomList 1000 <*> randomList 1000) $ \(l1, l2) -> shouldBe
          do testDiffs l1 l2
          do fmap (fmap reverseDiff) <$> testDiffs l2 l1
