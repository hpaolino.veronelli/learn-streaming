{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}

import Control.Monad
import qualified Data.Array as A
import Data.Maybe

parseCell :: Char -> Bool
parseCell = \case
  '.' -> False
  '#' -> True

type Pos = (Int, Int)

type Board = A.Array Pos Bool

parseArray :: [String] -> Board
parseArray ls =
  A.listArray ((0, 0), (length ls - 1, pred $ length $ head ls)) $
    parseCell <$> join ls

atPos :: Board -> Pos -> Maybe Bool
atPos a (y, x)
  | y >= ry = Nothing
  | otherwise = Just $ a A.! (y, mod x $ succ rx)
  where
    (_, (ry, rx)) = A.bounds a

repeatJump :: Pos -> Pos -> [Pos]
repeatJump (dy, dx) = iterate \(y, x) -> (y + dy, x + dx)

countTrees :: [String] -> Pos -> Int
countTrees ls d =
  length . filter id . catMaybes . takeWhile isJust
    . map (atPos $ parseArray ls)
    $ repeatJump d (0, 0)

main1 :: IO ()
main1 = do
  ls <- lines <$> readFile "input-3"
  print $ countTrees ls (1, 3)

main2 :: IO ()
main2 = do
  ls <- lines <$> readFile "input-3"
  print $
    product $
      fmap
        (countTrees ls)
        [ (1, 1)
        , (1, 3)
        , (1, 5)
        , (1, 7)
        , (2, 1)
        ]
