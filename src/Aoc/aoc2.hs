type Line = ((Int, Int), Char, String)

parseLine :: [Char] -> Line
parseLine xs =
  let [ranger, letterc, password] = words xs
      letter : _ = letterc
      (fromc, _ : toc) = break (== '-') ranger
   in ((read fromc, read toc), letter, password)

isOk1 :: Line -> Bool
isOk1 ((from, to), letter, password) =
  ((&&) <$> (>= from) <*> (<= to)) $
    length $ filter (== letter) password


main1 = do 
    print . length . filter isOk1 . fmap parseLine . lines =<< readFile "input-2"

isOk2 :: Line -> Bool
isOk2 ((from, to), letter, password) =
  ((/=) <$> elem from <*> elem to) $
     map fst . filter ((== letter) . snd) $ zip [1..] password
main2 = do 
    print . length . filter isOk2 . fmap parseLine . lines =<< readFile "input-2"
