{-# LANGUAGE ScopedTypeVariables #-}

import Data.List
import Data.Maybe (listToMaybe, maybeToList)
import Data.Map (Map)
import qualified Data.Map as M
import Control.Arrow
import Data.Foldable

type O a = a -> a -> Ordering

findMatch :: Num a => O a -> [a] -> [a] -> Maybe (a,a)
findMatch f xt@(x : xs) yt@(y : ys) = case f x y of
  EQ -> Just (x, y)
  LT -> findMatch f xs yt
  _ -> findMatch f xt ys
findMatch _ _ _ = Nothing 

findMatchWithPivot ::  Num a => O a  -> Int -> [a] -> [a] -> [[a]]
findMatchWithPivot f 0 xs ys = maybe [] (\(x,y)  -> [[x,y]]) $ findMatch f xs ys 
findMatchWithPivot f n xs ys = do 
    z:zs <- tails xs
    let zs' = (+ z) <$> zs
    result <- findMatchWithPivot f (pred n) zs' ys
    pure $ z : fmap (subtract z) result

type SP = (Int, Int)

findMatchM :: Int -> [SP] -> [Int] -> [SP]
findMatchM l xt@((x,p) : xs) yt@(y : ys) = 
  case compare xy l of
    EQ -> [(xy, p * y)]
    LT -> findMatchM l xs yt
    _ -> findMatchM l xt ys
   where 
      xy = x + y
findMatchM _ _ _ = []

findMatchWithPivotM ::   Int  -> Int -> [SP] -> [Int] -> Map Int Int -> Map Int Int
findMatchWithPivotM l 0 xs ys m = m <> foldMap (uncurry M.singleton) (findMatchM l xs ys)
findMatchWithPivotM f n xs ys m = fold $ do 
    (s,p) :zs <- tails xs
    let zs' = ((+ s) *** (* p)) <$> zs
    pure $ findMatchWithPivotM f (pred n) zs' ys m

main2 :: Int -> IO ()
main2 n = do  
  xs :: [Int] <- sort . fmap read . lines <$> readFile "input-1"
  case findMatchWithPivot  (\x y -> (x + y) `compare` 2020) n xs (reverse xs) of
    [] -> error "no one"
    (xs:_) -> print (xs, product xs)

main3 :: Int -> IO ()
main3 n = do  
  xs :: [Int] <- sort . fmap read . lines <$> readFile "input-1"
  print $ findMatchWithPivotM 2020 n (zip xs xs) (reverse xs) mempty