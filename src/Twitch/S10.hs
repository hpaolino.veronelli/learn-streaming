{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Twitch.S10 where

import Control.Monad.Managed -- https://hackage.haskell.org/package/managed-1.0.8
import Data.Attoparsec.Text
-- https://hackage.haskell.org/package/streaming

-- https://hackage.haskell.org/package/taggy-0.2.1
import Data.Foldable (Foldable (foldr1))
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Protolude hiding (Selector)
import Streaming
import Streaming.Internal
import qualified Streaming.Prelude as S
import System.IO (hClose, hIsEOF, openBinaryFile)
import Test.Hspec (describe, hspec, it, shouldBe)
import Text.Pretty.Simple
import Text.Taggy (Attribute (..), Tag (..), tag)

-- safely stream a file in text chuncks
streamFile :: MonadManaged m => FilePath -> Stream (Of Text) m Int
streamFile path = Effect do
  h <- managed do
    bracket
      do openBinaryFile path ReadMode
      do hClose
  let loop count = Effect $ liftIO do
        t <- T.hGetChunk h
        eof <- hIsEOF h
        pure do
          S.yield t
          if eof
            then Effect $ liftIO do
              hClose h
              pure $ Return count
            else loop (succ count)
  pure $ loop 0

-- transform a stream of text chunks into a stream of 'a' and return the unparsed stream
type ParseStream m a r = Stream (Of Text) m r -> Stream (Of a) m (Stream (Of Text) m r)

-- promote an attoparsec text parser to a streaming transforming parser on chunks of text
streamParser :: forall m a r. Monad m => Parser a -> ParseStream m a r
streamParser parser = start Nothing
  where
    start Nothing = loop (parse parser "") True identity -- start parser with no text
    start (Just rest) = loop (parse parser rest) False $ S.cons rest -- start parser with leftovers
    loop (Partial continue) isFresh rollback s = Effect do
      lr <- S.next s
      pure $ case lr of
        Right (t, s') ->
          loop (continue t) False (rollback . S.cons t) s' -- feed and remember
        Left r ->
          if isFresh
            then Return (Return r) -- good work
            else loop (continue "") False rollback s -- make it fail
    loop Fail {} _ rollback s = pure $ rollback s
    loop (Done rest x) _ _ s = do
      S.yield x
      s & start do
        if T.null rest
          then Nothing
          else Just rest

-- promote the Tag parser , from taggy lib
tags :: Monad m => ParseStream m Tag r
tags = streamParser (tag True)

-- content of an opening tag
data Context = Context
  { contextName :: Text -- tagname
  , contextAttributes :: [Attribute] -- attributes
  }
  deriving (Eq, Show)

-- dirty rose tree , Text can be there or not, "" is not
data Tree = Node Context Text [Tree] deriving (Eq, Show)

setText :: Text -> Tree -> Tree
setText t (Node y _ xs) = Node y t xs

-- tags that count for us
data STag = O Context | C | T Text

-- remove self closing tags
normalizeTags :: Monad m => Stream (Of Tag) m r -> Stream (Of STag) m r
normalizeTags = flip S.for \case
  TagOpen name attrs False -> S.yield $ O $ Context name attrs
  TagOpen name attrs True -> do
    S.yield $ O $ Context name attrs
    S.yield C
  TagClose _ -> S.yield C
  TagText t -> S.yield $ T t
  _ -> pure ()

-- stream same level nodes , return the stream of tags as soon as the level goes under 0
streamTrees :: Functor m => Stream (Of STag) m r -> Stream (Of Tree) m (Either r (Stream (Of STag) m r))
streamTrees = go Nothing -- Boot
  where
    reverseNode :: Tree -> Tree
    reverseNode (Node x t xs) = Node x t $ reverse xs
    go _ (Return r) = Return $ Left r
    go mt (Effect m) = Effect $ go mt <$> m
    -- entering new context
    go mt (Step (O x :> rest)) = go
      do Just (Node x "" [], maybe [] (uncurry (:)) mt)
      do rest
    -- closing a context
    go mt (Step (C :> rest)) = case mt of
      -- closing the zero level context
      Nothing -> Return $ Right $ Step $ C :> rest
      -- stream a tree and attack the rest as fresh
      Just (x, []) -> Step $ reverseNode x :> go Nothing rest
      -- closing a inner context, push the baby in
      Just (x, Node y t xs : ys) -> go
        do Just (Node y t $ reverseNode x : xs, ys)
        do rest
    go mt (Step (T t :> rest)) = go
      do first (setText $ T.strip t) <$> mt
      do rest

-- break when it's time to collect trees, 'c' is *valid* for the next segment of trees
type Selector c m r = c -> Stream (Of STag) m r -> (forall f. Stream f m (Either r (c, Stream (Of STag) m r)))

-- a flower, c is the stem  up to the node context, node forest is streamed
data StreamingFlower c m r = StreamingFlower c (Stream (Of Tree) m r)
  deriving (Functor)

-- coroutine to alternate context (stemming) and node parsing phases (bloom)
-- notice the (>>=) to concatenate the 2 streaming
streamFlowers
  :: forall m c r.
  Monad m
  => c -- selector state
  -> Selector c m r -- stream breaker for next stem
  -> Stream (Of STag) m r -- stream of tags
  -> Stream (StreamingFlower c m) m r -- stream of flowers
streamFlowers selectorState selector stream =
  selector selectorState stream
    >>= \case
      Right (c, s) ->
        Step $
          StreamingFlower c $
            streamTrees s <&> \case
              Left x -> Return x
              Right stream -> streamFlowers c selector stream
      Left r -> Return r

-- a StreamingFlower with its forest collapsed to a list , maybe use a Map Text (Attribute, Tree) to select properties?
data Flower c = Flower c [Tree] deriving (Show, Eq)

-- collapse the internal stream of trees, notice we need to *run* the monadic layers, as with all folds
runFlower :: Monad m => StreamingFlower c m r -> m (Of (Flower c) r)
runFlower (StreamingFlower n s) = first (Flower n) <$> S.toList s

--------- end of library -----------------------------------------

--------- a simple application layer -----------------------------

-- a possible vision of the context for a node, poor but simple way
data SimpleStem = SimpleStem
  { stemDepth :: Int -- number of contexts, support for fast depth analisys
  , stemContent :: [Context] -- reversed context stack
  }
  deriving (Show, Eq)

popContext :: SimpleStem -> SimpleStem
popContext (SimpleStem n (_ : xs)) = SimpleStem (pred n) xs

pushContext :: Context -> SimpleStem -> SimpleStem
pushContext x (SimpleStem n xs) = SimpleStem (succ n) (x : xs)

emptyStem :: SimpleStem
emptyStem = SimpleStem 0 []

-- a selector select flowers on tag name,  notice passing the SimpleStem through
getTag :: Monad m => Text -> Selector SimpleStem m r
getTag name = go False
  where
    go _ _ (Return r) = Return $ Left r
    go t l (Effect m) = Effect $ go t l <$> m
    go t l (Step (T _ :> s)) = go t l s
    go _ l (Step (C :> s)) = go False (popContext l) s
    go t l s@(Step (O c@(Context x _) :> s'))
      | x == name = go True (pushContext c l) s'
      | t = Return $ Right (l, s)
      | otherwise = go False (pushContext c l) s'

------------  dedicated to http://aiweb.cs.washington.edu/research/projects/xmltk/xmldata/data/mondial/mondial-3.0.xml -------------------

-- we want to stream italian cities

-- very inefficient way of collapsing information, stems change incrementally, we should reuse past collapsing
-- in fact, all italians cities comes together  but we ignore it
cleanStem :: Flower SimpleStem -> (Map Text (Maybe Text), [Tree])
cleanStem
  ( Flower
      ( SimpleStem
          _
          ( _city
              : Context "country" countryAttrs
              : _
            )
        )
      n
    ) = (cs', n)
    where
      cs' = M.fromList [("country", findName countryAttrs)]
cleanStem
  ( Flower
      ( SimpleStem
          _
          ( _city
              : Context "province" provinceAttrs
              : Context "country" countryAttrs
              : _
            )
        )
      n
    ) = (cs', n)
    where
      cs' = M.fromList [("country", findName countryAttrs), ("province", findName provinceAttrs)]
cleanStem (Flower (SimpleStem _ _) n) = (mempty, n)

findName :: [Attribute] -> Maybe Text
findName (Attribute "name" name : _) = Just name
findName (_ : xs) = findName xs

getCities :: Monad m => Stream (Of Tag) m r -> Stream (Of (Flower SimpleStem)) m r
getCities = S.mapped runFlower . streamFlowers emptyStem (getTag "city") . normalizeTags

italianCities :: MonadManaged m => FilePath -> Stream (Of (Map Text (Maybe Text), [Tree])) m (Stream (Of Text) m Int)
italianCities = S.filter ((== Just (Just "Italy")) . M.lookup "country" . fst) . S.map cleanStem . getCities . tags . streamFile

pPrintStream :: (Show a, MonadIO m) => Stream (Of a) m r -> Stream (Of ()) m r
pPrintStream = S.mapM $ \x -> liftIO $ void $ pPrint x >> getLine

main :: IO ()
main = runManaged $ void $ S.effects $ pPrintStream $ italianCities "mondial-3.0.xml"

----------------- testing by depth selector   ---------------------

depth :: (Monad m) => Int -> Selector SimpleStem m r
depth n = go
  where
    go (SimpleStem 0 []) (Return r) = Return $ Left r
    go l (Effect m) = Effect $ go l <$> m
    go l (Step (C :> s)) = go (popContext l) s
    go l s@(Step (O x :> s'))
      | stemDepth l < n = go (pushContext x l) s'
      | otherwise = Return $ Right (l, s)

flowers :: (Monad m) => Int -> Stream (Of STag) m r -> Stream (StreamingFlower SimpleStem m) m r
flowers n = streamFlowers emptyStem $ depth n

listOfFlowers :: Monad m => Stream (StreamingFlower c m) m r -> m (Of [Flower c] r)
listOfFlowers = S.toList . S.mapped runFlower

testFlowers :: Int -> [STag] -> Of [Flower SimpleStem] ()
testFlowers n xs = runIdentity $ listOfFlowers $ flowers n $ S.each xs

naC :: Show a => a -> Context
naC n = Context (show n) []

naO :: Int -> STag
naO = O . naC

naNode :: Int -> [Tree] -> Tree
naNode n = Node (naC n) ""

test :: IO ()
test = hspec do
  describe "flowers" do
    it "gets no flowers from emptyness" $
      shouldBe
        do testFlowers 0 []
        do [] :> ()
    it "gets a flower" $
      shouldBe
        do testFlowers 0 [naO 1, C]
        do [Flower emptyStem [naNode 1 []]] :> ()
    it "gets a bigger flower" $
      shouldBe
        do testFlowers 0 [naO 1, naO 2, C, C]
        do [Flower emptyStem [naNode 1 [naNode 2 []]]] :> ()
    it "gets a stemmed flower" $
      shouldBe
        do testFlowers 1 [naO 1, naO 2, C, C]
        do [Flower (SimpleStem 1 [naC 1]) [naNode 2 []]] :> ()
    it "gets a bigger stemmed flower" $
      shouldBe
        do testFlowers 1 [naO 1, naO 2, C, naO 3, C, C]
        do [Flower (SimpleStem 1 [naC 1]) [naNode 2 [], naNode 3 []]] :> ()
    it "gets 2 stemmed flowers" $
      shouldBe
        do testFlowers 1 [naO 1, naO 2, C, naO 3, C, C, naO 4, naO 5, C, C]
        do
          [ Flower (SimpleStem 1 [naC 1]) [naNode 2 [], naNode 3 []]
            , Flower (SimpleStem 1 [naC 4]) [naNode 5 []]
            ]
            :> ()

-- copy :: Monad m => Stream (Of a) m r -> Stream (Of a) (Stream (Of a) m) r
-- copy str = case str of
--   Return r -> Return r
--   Effect m -> Effect $ copy <$> lift m
--   Step (a :> rest) ->
--     Effect $
--       Step $
--         a
--           :> do Return $ Step $ a :> copy rest

-- testCopy :: IO (Of [[Int]] (Of [[Int]] ()))
-- testCopy =
--   S.toList . mapped S.toList . chunksOf 5 $
--     S.toList . mapped S.toList . chunksOf 3 $
--       S.copy $
--         S.mapM (\x -> print x >> pure x) $
--           S.each [1 .. 10 :: Int]

data Rollback = Rollback | Reset | Exit | Continue

data Control a x = Control a (Rollback -> x) deriving (Functor)

rollbacking :: forall m a r. (Monad m, Show a) => Stream (Of a) m r -> Stream (Control a) m (Either r (Stream (Of a) m r))
rollbacking = go identity
  where
    go
      :: (Stream (Of a) m r -> Stream (Of a) m r)
      -> Stream (Of a) m r
      -> Stream (Control a) m (Either r (Stream (Of a) m r))
    go _ (Return r) = Return $ Left r
    go f (Effect m) = Effect $ go f <$> m
    go f (Step (x :> rest)) =
      let back = f . S.cons x
       in Step $
            Control x $ \case
              Continue -> go back rest
              Reset -> go identity rest
              Exit -> Return $ Right $ back rest
              Rollback -> rollbacking $ back rest

data SParserResult a b = SParserDone b | SParserFailed | Incomplete (SParser a b) deriving (Functor)

reportParserDone (SParserDone x) = Just x
reportParserDone _ = Nothing

data SParserControl a = SPNext a | SPRollback

data SParser a b = SParser (SParserControl a -> SParserResult a b) | AlternativeParser (SParser a b) (SParser a b) deriving (Functor)

instance Applicative (SParser a) where
  pure _x = notImplemented -- SParser \case 
    -- SNext y -> SParserDone x
  SParser f <*> x = SParser $ \y ->
    case f y of
      SParserDone f' -> Incomplete $ f' <$> x
      SParserFailed -> SParserFailed
      Incomplete p' -> Incomplete $ p' <*> x
  AlternativeParser p q <*> x = AlternativeParser (p <*> x) (q <*> x)

instance Monad (SParser a) where
  SParser x >>= f = SParser $ \y -> case x y of
    SParserFailed -> SParserFailed
    SParserDone x' -> Incomplete $ f x'
    Incomplete p' -> Incomplete $ p' >>= f
  AlternativeParser p q >>= f = AlternativeParser (p >>= f) (q >>= f)

instance Alternative (SParser a) where
  a <|> b = AlternativeParser a b
  empty = SParser $ const SParserFailed

token :: Eq a => a -> SParser a a
token x = SParser $ \case
  SPNext y -> if x == y then SParserDone x else SParserFailed
  SPRollback -> SParserFailed 

tokens :: Eq a => [a] -> SParser a [a]
tokens = mapM token

streamParse :: forall m a b r. (Functor m) => SParser a b -> Stream (Control a) m r -> Stream (Of b) m r
streamParse p0 = go [p0]
  where
    go :: (Functor m) => [SParser a b] -> Stream (Control a) m r -> Stream (Of b) m r
    go _ps (Return r) = Return r
    go ps (Effect m) = Effect $ go ps <$> m
    go (AlternativeParser p q : alt) x = go (p : q : alt) x
    go (SParser p : ps) (Step (Control x rest)) = case p (SPNext x) of
      Incomplete p' -> go (p' : ps) $ rest Continue
      SParserDone s -> Step (s :> go [p0] (rest Reset))
      SParserFailed -> case p SPRollback of
        SParserFailed -> case ps of
          [] -> go (panic "roll back exited") $ rest Exit
          _ -> go ps $ rest Rollback
        Incomplete p' -> go (p' : ps) $ rest Rollback

data CTree a = CTree a (CForest a) deriving (Show)

type CForest a = [CTree a]

parseCTree :: Eq a => CTree a -> SParser a [a]
parseCTree r = go identity (r, [])
  where
    go f (CTree c cs, qtss) = SParser $ \case
      SPRollback -> case qtss of
        ((fqt, qt) : qts) -> Incomplete $ go fqt (qt, qts)
        [] -> SParserFailed
      SPNext x ->
        let rb = f . (x :)
         in if x == c
              then case cs of
                [] -> SParserDone $ rb []
                (c' : cs') -> Incomplete $ go rb (c', (f, CTree c cs') : qtss)
              else SParserFailed

parseCTree' :: Eq a => CTree a -> SParser a [a]
parseCTree' r = go identity (r, [])
  where
    go f (CTree c cs, qtss) = SParser $ \case
      SPRollback -> case qtss of
        ((fqt, qt) : qts) -> Incomplete $ go fqt (qt, qts)
        [] -> SParserFailed
      SPNext x ->
        let rb = f . (x :)
         in if x == c
              then case cs of
                [] -> SParserDone $ rb []
                _ -> Incomplete $ foldr1 AlternativeParser (map (\cr -> go rb (cr, (f, CTree c []) : qtss)) cs)
              else SParserFailed

--     [] -> go [] $ rest Exit
--     _ -> go ps $ rest Rollback

testParser :: Monad m => Stream (Of Char) m r -> Stream (Of [Char]) m (Either r (Stream (Of Char) m r))
testParser = abcORabx . rollbacking

abcORabx :: Functor m => Stream (Control Char) m r -> Stream (Of [Char]) m r
abcORabx = streamParse $
  AlternativeParser
    do parseCTree $ CTree 'a' [CTree 'b' [], CTree 'c' []] -- [CTree 'f' [CTree 'c' []], CTree 'b' [CTree 'x' []]]
    do parseCTree $ CTree 't' [CTree 'b' [CTree 'c' []], CTree 'c' []] -- [CTree 'f' [CTree 'c' []], CTree 'b' [CTree 'x' []]]

-- runParser :: [a] -> ([[a]], [a])
runParser xs = runIdentity do
  (r :> rest) <- S.toList $ testParser $ S.each xs
  q <- case rest of
    Left () -> pure []
    Right q -> do
      v :> () <- S.toList q
      pure v
  return (r, q)

-- dream = do
--   token "a"
--   do
--     (<|>)
--       do token "f" >> token "c"
--       do token "b" >> token "x"
