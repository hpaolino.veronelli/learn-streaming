{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Twitch.S12 where

-- https://hackage.haskell.org/package/streaming

import qualified Control.Foldl as L
import Control.Lens (APrism, APrism', Prism', makePrisms, (^?))
import Control.Monad.Fail
import Control.Monad.Managed
import Control.Monad.Writer (WriterT (WriterT), runWriterT, tell)
import Data.List.NonEmpty (tail)
import Protolude
import Streaming
import Streaming.Internal
import Streaming.Learn (streamFile)
import Streaming.Parser
import qualified Streaming.Prelude as S
import Test.Hspec
import qualified Text.Parsec as P
import qualified Text.ParserCombinators.Parsec.Pos as P
import Text.Taggy

data Validation c p = Partial (Validate c p) (Validate c p) | Accept p | Drop deriving (Functor)

newtype Validate c p = Validate (c -> Validation c p) deriving (Functor)

matchAll :: p -> Validate c p
matchAll p = Validate $ const $ Accept p

matchNone :: Validate c p
matchNone = Validate $ const Drop

data Judge c p = Judge {judgeValidators :: NonEmpty (Validate c p), judgeDepth :: Int}

data What p = Skip | Dig | Parse p

push :: c -> Judge c p -> (What p, Judge c p)
push c j@(Judge (Validate x :| xs) n) = case x c of
  Drop -> (Skip, j)
  Accept p -> (Parse p, j)
  Partial x' y -> (Dig, Judge (y :| x' : xs) $ succ n)

pop :: Judge c p -> Maybe (Judge c p)
pop Judge {..} = Judge <$> nonEmpty (tail judgeValidators) <*> pure (judgeDepth - 1)

type P m a b r = Stream (Of a) m r -> m (Maybe b, Stream (Of a) m r)

type B m a r = Stream (Of a) m r -> Stream (Of a) m (Stream (Of a) m r)

data J c = Pop | Push c | Stay

embedP :: (Monad m, Show a) => (a -> J c) -> Judge c (P m a b (Stream (Of a) m r)) -> B m a r -> Stream (Of a) m r -> Stream (Of b) m r
embedP _ _ _ (Return r) = Return r
embedP kc f b (Effect m) = Effect $ embedP kc f b <$> m
embedP kc f breaker x@(Step (y :> rest)) =
  -- traceShow y $
  case kc y of
    Stay -> embedP kc f breaker rest
    Pop -> case pop f of
      Nothing -> panic "to be handled as Return "
      Just f' -> embedP kc f' breaker rest
    Push c -> case push c f of
      (Dig, f') -> embedP kc f' breaker rest
      (Skip, f') -> Effect $ do
        s <- S.effects $ breaker x
        pure $ embedP kc f' breaker s
      (Parse p, f') -> Effect $ do
        (b, cs) <- p $ breaker x
        s <- S.effects cs
        pure $ case b of
          Nothing -> embedP kc f' breaker s
          Just b -> Step $ b :> embedP kc f' breaker s

-- replicateHead :: Monad m => Stream (Of a) m b -> Stream (Of a) m b
-- replicateHead (Return r) = Return r
-- replicateHead (Effect m) = Effect $ replicateHead <$> m
-- replicateHead (Step (x :> rest)) = S.yield x >> S.yield x >> rest

--  Parsec as stream consumer

type Tokens m a r = Stream (Of a) m r

type RemTokens m a r = WriterT (Last (Tokens m a r)) m

instance Monad m => P.Stream (Tokens m a r) (RemTokens m a r) a where
  uncons (Return _) = pure Nothing
  uncons (Effect m) = lift m >>= P.uncons
  uncons (Step (x :> rest)) = tell (Last $ Just rest) $> Just (x, rest)

type Parser r m a b = P.ParsecT (Tokens m a r) () (RemTokens m a r) b

consumeParser :: Monad m => Parser r m a b -> Tokens m a r -> m (Maybe b, Tokens m a r) -- , Tokens m a r)
consumeParser p s = fmap (fmap $ getPartialLast s) $
  runWriterT $ do
    r <- P.runParsecT p $ P.State s (P.initialPos "test") ()
    result <- case r of
      P.Empty r' -> fmap (False,) <$> r'
      P.Consumed r' -> fmap (True,) <$> r'
    pure $ case result of
      P.Ok (_, x) (P.State _ _ ()) _e -> Just x -- Just (x, if c then Just s' else Nothing)
      P.Error _e -> Nothing

getPartialLast :: Tokens m a r -> Last (Tokens m a r) -> Tokens m a r
getPartialLast old (Last Nothing) = old
getPartialLast _ (Last (Just s)) = s

------------- taggy --------------------

-- breaking out a node

countLevel :: Int -> Tag -> Int
countLevel n (TagClose _) = pred n
countLevel n (TagOpen _ _ False) = succ n
countLevel n _ = n

breaker :: Monad m => Stream (Of Tag) m r -> Stream (Of Tag) m (Stream (Of Tag) m r)
breaker = go 0
  where
    go _ (Return r) = Return (Return r)
    go n (Effect m) = Effect $ go n <$> m
    go n Step (t :> rest) =
      S.cons
        t
        let n' = countLevel n t
         in if n' <= 0
              then Return rest
              else go n' rest
-- parsec and tags

------- against Tags --------------------------------
makePrisms ''Tag
type TagParser r m a = Parser r m Tag a

-- tagText :: Monad m => TagParser r m Text
tagP :: P.Stream s m Tag => Prism' Tag a -> P.ParsecT s u m a
tagP p = do
  x <- P.anyToken
  case x ^? p of
    Just t -> pure t
    _ -> fail "wanted a different tag"

-- skipText :: Monad m => TagParser r m ()
skipText :: P.Stream s m Tag => P.ParsecT s u m Text
skipText = do
  mconcat <$> P.many1 (P.try $ tagP _TagText) <|> pure ""

aLeaf :: P.Stream s m Tag => P.ParsecT s u m Tree
aLeaf = do
  TagOpen n attrs True <- P.anyToken
  pure $ Tree n "" attrs []

-- aTag :: Monad m => TagParser r m Tree
aTag :: P.Stream s m Tag => P.ParsecT s u m Tree
aTag = do
  skipText
  (n, attrs, False) <- tagP _TagOpen
  v <- children
  tagP _TagClose
  skipText
  pure case v of
    Left xs -> Tree n "" attrs xs
    Right t -> Tree n t attrs []

aTree :: P.Stream s m Tag => P.ParsecT s u m Tree
aTree = skipText >> (P.try aLeaf <|> aTag)

children :: P.Stream s m Tag => P.ParsecT s u m (Either [Tree] Text)
children = do
  text <- skipText
  Left <$> P.many1 (P.try aTree) <|> pure (Right text)
-- context
data Context = Context
  { contextName :: Text -- tagname
  , contextAttributes :: [Attribute] -- attributes
  }
  deriving (Eq, Show)

mkContext :: Tag -> J Context
mkContext (TagOpen contextName contextAttributes False) = Push $ Context {..}
mkContext (TagOpen _ _ True) = panic "self closing not supported, normalize pls"
mkContext (TagClose _) = Pop
mkContext _ = Stay

findName :: [Attribute] -> Maybe Text
findName (Attribute "name" name : _) = Just name
findName (_ : xs) = findName xs

data Tree = Tree Text Text [Attribute] [Tree] deriving (Show, Eq)

cityName :: Monad m => P m Tag Tree (Stream (Of Tag) m r)
cityName = consumeParser aTag

data City = City Text (Maybe Text) Tree deriving (Show)

-- cities :: Judge Context (Parser r m Tag (Text, Text))
cities :: forall m r. Monad m => Judge Context (P m Tag City (Stream (Of Tag) m r))
cities = Judge (country :| []) 1
  where
    country = Validate \case
      Context "country" attrs -> case findName attrs of
        Nothing -> Drop
        Just n -> Partial country $ province n
      Context _ _ -> Partial country country

province :: Monad m => Text -> Validate Context (P m Tag City (Stream (Of Tag) m r))
province countryName = Validate \case
  Context "province" attrs -> case findName attrs of
    Nothing -> Drop
    Just n -> Partial (province countryName) $ city countryName n
  Context "city" _ -> Accept $ fmap (first $ fmap (City countryName Nothing)) <$> cityName
  _ -> Partial (province countryName) (province countryName)

city :: Monad m => Text -> Text -> Validate Context (P m Tag City (Stream (Of Tag) m r))
city countryName provinceName = Validate \case
  Context "city" _ -> Accept $ fmap (first $ fmap (City countryName $ Just provinceName)) <$> cityName
  _ -> Partial (city countryName provinceName) (city countryName provinceName)

-- city :: Text -> Validate Context (Parser r m Tag (Text, Text))
-- city countryName = Validate _

--     Context "city" _  -> Accept $ (countryName,) <$> consumeParser cityName


produceCities :: Monad m => Stream (Of Tag) m r -> Stream (Of Tree) m r
produceCities = embedP mkContext (Judge (matchAll (consumeParser aTag) :| []) 1) breaker



--- main
normalizeTags :: Monad m => Stream (Of Tag) m r -> Stream (Of Tag) m r
normalizeTags s = S.for s \case
  TagOpen name attributes True -> do
    S.yield $ TagOpen name attributes False
    S.yield $ TagText ""
    S.yield $ TagClose name
  x -> S.yield x

fromXMLF :: MonadManaged m => FilePath -> Stream (Of City) m (Stream (Of Text) m Int)
fromXMLF =
  produceB
    . normalizeTags
    . tags
    . streamFile

fromXML :: Monad m => Stream (Of Text) m r -> Stream (Of City) m (Stream (Of Text) m r)
fromXML =
  produceB
    . normalizeTags
    . tags

t1 :: Monad m => Stream (Of Text) m ()
t1 = S.each ["<a > <b/> </a>"]

produceB :: Monad m => Stream (Of Tag) m r -> Stream (Of City) m r
produceB = embedP mkContext cities breaker

lister :: Monad m => P m Tag [Tag] (Stream (Of Tag) m r)
lister s = (\(x :> r) -> (Just x, Return r)) <$> S.toList s

testTreeParser :: P.Stream s Identity Tag => s -> Either P.ParseError Tree
testTreeParser = P.runParser aTree () "test"


main :: IO ()
main = hspec do
  describe "aTag" $ do
    it "parses a Leaf" $
      shouldBe
        do testTreeParser [TagOpen "a" [] False, TagText "b", TagClose "a"]
        do Right $ Tree "a" "b" [] []
    it "parses a nested Leaf" $
      shouldBe
        do testTreeParser [TagOpen "a" [] False, TagOpen "b" [] False, TagClose "b", TagClose "a"]
        do Right $ Tree "a" "" [] [Tree "b" "" [] []]
