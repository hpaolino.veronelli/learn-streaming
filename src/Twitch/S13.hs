{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Twitch.S13 where

-- https://hackage.haskell.org/package/streaming

-- https://hackage.haskell.org/package/taggy-0.2.1

import Data.List.NonEmpty (tail)
import Protolude hiding (Selector)
import Streaming
import Streaming.Internal
import qualified Text.Parsec as P
import qualified Text.Parsec.Pos as P
import Text.Taggy
import Control.Monad.Writer (WriterT, runWriterT, tell)
data Context = Context
  { contextName :: Text -- tagname
  , contextAttributes :: [Attribute] -- attributes
  }
  deriving (Eq, Show)

data Validation p = Partial (Validate p) (Validate p) | Accept p | Drop

newtype Validate p = Validate (Context -> Validation p)

matchAll :: p -> Validate p
matchAll p = Validate $ const $ Accept p

matchNone :: Validate p
matchNone = Validate $ const Drop

data Judge p = Judge {judgeValidators :: NonEmpty (Validate p), judgeDepth :: Int}

data What p = Skip | Dig | Parse p

push :: Context -> Judge p -> (What p, Judge p)
push c j@(Judge (Validate x :| xs) n) = case x c of
  Drop -> (Skip, j)
  Accept p -> (Parse p, j)
  Partial x' y -> (Dig, Judge (y :| x' : xs) $ succ n)

pop :: Judge p -> Maybe (Judge p)
pop Judge {..} = Judge <$> nonEmpty (tail judgeValidators) <*> pure (judgeDepth - 1)


 

findName :: [Attribute] -> Maybe Text
findName (Attribute "name" name : _) = Just name
findName (_ : xs) = findName xs

cityName :: Parser r m Tag Text 
cityName = notImplemented 

cities :: Judge (Parser r m Tag (Text, Text))
cities = Judge (country :| []) 1 
  where 
    country = Validate \case
        Context "country" attrs  -> case 
          findName attrs of
            Nothing -> Drop 
            Just n -> Partial country $ city n 
        _ -> Partial country country 
    city countryName = Validate \case
        Context "city" _  -> Accept $ (countryName,) <$> cityName 
        _ -> Partial (city countryName) (city countryName)




type Tokens m a r = Stream (Of a) m r
type RemTokens m a r = WriterT (Last (Tokens m a r)) m

instance Monad m => P.Stream (Tokens m a r) (RemTokens m a r) a where
  uncons (Return _) = pure Nothing
  uncons (Effect m) = lift m >>= P.uncons
  uncons (Step (x :> rest)) = tell (Last $ Just rest) $> Just (x, rest)

type Parser r (m :: * -> *) a b = P.ParsecT (Tokens m a r) () (RemTokens m a r)  b

consumeParser :: Monad m => Parser r m a b -> Tokens m a r -> m (Maybe b, Maybe (Tokens m a r)) -- , Tokens m a r)
consumeParser p s = fmap (fmap getLast) $ runWriterT $ do
  r <- P.runParsecT p $ P.State s (P.initialPos "test") ()
  result <- case r of
    P.Empty r' -> fmap (False,) <$> r'
    P.Consumed r' -> fmap (True,) <$> r'
  pure $ case result of
    P.Ok (_ , x) (P.State _ _ ()) _e -> Just x -- Just (x, if c then Just s' else Nothing)
    P.Error _e -> Nothing

breaker :: Functor m => Judge (Parser r m Tag a) -> Tokens m Tag r -> Tokens m a  (Tokens m Tag r)
breaker _ (Return r) = Return $ Return r
breaker j (Effect m) = Effect $ breaker j  <$> m 
breaker j (Step (t :> rest)) = case t of 
  TagOpen name attrs False -> let 
    (w,j') = push (Context name attrs) j 
    in case w of 
      Dig -> breaker j' rest 
      Skip ->  notImplemented  
        -- rest' <- S. breakNode n

