{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}

module Twitch.S6 where

import qualified Data.Map as M
import Protolude
import Text.Taggy.Types
import qualified Data.Text as T
import Test.Hspec

data Context = Context
  { contextName :: Text -- tagname
  , contextAttributes :: [Attribute] -- attributes 
  }
  deriving (Eq, Show)

-- this encoding doesn't support brotherhood count
-- for that we need to rewrite *self* as well
newtype Validate = Validate (Context -> Maybe (Bool, Validate)) 

matchAll :: Validate
matchAll = Validate $ const $ Just (True, matchAll)

matchNone :: Validate
matchNone = Validate $ const Nothing 

newtype XPath = XPath (Map Position Validate) -- from 0 to lenght xpath - 1 , no holes

type Position = Int

data Modify = Modify Position [Context] 

-- oversophisticate, to gain little shortcut, we could use non integrated tag stream instead
updateXPath :: Position -> [Context] -> XPath -> (Bool, XPath)
updateXPath  p xs (XPath q) = go True p xs $ XPath $ fst $ M.spanAntitone (<= p) q
    where 
    go :: Bool -> Position -> [Context] -> XPath -> (Bool, XPath)
    go c _ [] q = (c, q)
    go _ p (x : xs) xp@(XPath q) = case M.lookup p q of
        Nothing -> (False, xp) -- here we tolarate absurd queries together with good ones
        Just (Validate r) -> case r x of
            -- throw away the upper part (up down encoding should make it for free)
            Nothing -> (False, xp)
            Just (c, r') -> go c (succ p) xs
                do XPath $ M.insert (succ p) r' q


-- testUpdateXPath ::  [(Position, [Context])] -> Bool
testUpdateXPath :: Validate -> [(Position, [Context])] -> [Bool]
testUpdateXPath e = snd . mapAccumL f (XPath $ M.singleton 0 e)
    where f x (p,cs) = swap $ updateXPath p cs x  




-------------------  simple path selection ---------------------------------

data Glob 
    = UpTo Text  -- glob everything up to 
    | Segment Text 
    | Jolly 
    deriving (Show, Eq)

readGlob :: [Text] -> [Glob]
readGlob [""] = [] 
readGlob [] = []
readGlob ("":t:xs) = UpTo t : readGlob xs 
readGlob ("*":xs) = Jolly : readGlob xs 
readGlob (t:xs) = Segment t : readGlob xs 
-- readGlob _ = panic "read a blog instead"

readQuery :: Text -> Query
readQuery t = Query do  
    case T.head t of 
        '/' -> tail $ readGlob $ T.splitOn "/" $ "_" <> t
        _ -> readGlob $  T.splitOn "/" $ "/" <> t


newtype Query = Query [Glob]
  deriving (Eq, Show)

matchGlob :: Query -> Validate
matchGlob (Query []) = matchAll
matchGlob (Query (x : xs)) = fix $ \e -> Validate \(Context tn _) ->
  let step = (null xs , matchGlob $ Query xs)
   in case x of
        Segment t -> if t == tn then Just step else Nothing
        Jolly -> Just step
        UpTo t -> Just if t == tn then step else (False, e)

main :: IO ()
main = hspec $ do
    describe "readQuery" do 
        it "can parse a segment" $ 
            shouldBe 
                do readQuery "/segment"
                do Query [Segment "segment"]
        it "can parse a upto segment" $ 
            shouldBe 
                do readQuery "segment"
                do Query [UpTo "segment"]
        it "can parse a random one" $ 
            shouldBe 
                do readQuery "segment//ciao/mamma/*//piero"
                do Query  [UpTo "segment",UpTo "ciao",Segment "mamma",Jolly,UpTo "piero"] 
    describe "matchGlob"  do 
        it "can match all" $ 
            shouldBe 
                do testUpdateXPath (matchGlob $ readQuery "") 
                    []
                do []
        it "can match a segment" $ 
            shouldBe 
                do testUpdateXPath (matchGlob $ readQuery "/segment") 
                    [(0, [Context "segment" []])]
                do [True]
        it "can match a segment" $ 
            shouldBe 
                do testUpdateXPath (matchGlob $ readQuery "segment") 
                    [(0, [Context "segment" []])]
                do [True]
        it "can match a segment root" $ 
            shouldBe 
                do testUpdateXPath (matchGlob $ readQuery "segment") 
                    [ (0, [Context "segment" [], Context "else" []])
                    ]
                do [True]
        it "can match a segment root on 2 nodes" $ 
            shouldBe 
                do testUpdateXPath (matchGlob $ readQuery "segment") 
                    [ (0, [Context "segment" [], Context "else" []])
                    , (1, [Context "more else" []])
                    ]
                do [True, True]

        {-
        <segment>
            <else></else>
            <more else></more else>
        </segment>
        <no segment></no segment>
        <segment>
            <more else></more else>
        </segment>
        -}
        it "can match a segment root on 4 nodes, with failing " $ 
            shouldBe 
                do testUpdateXPath (matchGlob $ readQuery "/segment") 
                    [ (0, [Context "segment" [], Context "else" []]) -- <segment><else>
                    , (1, [Context "more else" []]) -- % <more else>
                    , (0, [Context "no segment" []]) -- <no segment>
                    , (0, [Context "segment" [],  Context "more else" []]) -- <segment><more else>
                    ]
                do [True, True, False,  True]

data Path = Path [Context] Text 
