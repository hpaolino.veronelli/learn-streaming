{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Twitch.S4 where

import Data.Attoparsec.Text
import qualified Data.Text as T
import Protolude
import Streaming
import Streaming.Internal (Stream (..))
import Streaming.Prelude (next)

yield :: a -> Stream (Of a) m r -> Stream (Of a) m r
yield x = Step . (x :>)

each :: [a] -> Stream (Of a) m ()
each = foldr yield $ Return ()

type ParseStream m a = Stream (Of Text) m () -> Stream (Of a) m (Stream (Of Text) m ())

type Restore m a = Stream (Of Text) m () -> Stream (Of Text) m ()

{- Data.Attoparsec.Text.Internal.parse
:: Parser a -> Text -> Result a
-}
{-
data Result r
  = Fail Text [String] String
  | Partial (Text -> Result r)
  | Done Text r

Streaming.Prelude.next
:: Monad m
=> Stream (Of Text) m ()
-> m (Either () (Text, Stream (Of Text) m ()))
-}

streamParser :: forall a m. Monad m => Parser a -> ParseStream m a
streamParser parser = start Nothing
  where
    --
    start
      :: Maybe Text
      -> ParseStream m a
    start Nothing = loop (parse parser "") True identity -- start parser with no text
    start (Just rest) = loop (parse parser rest) False $ yield rest -- start parser with leftovers
    loop
      :: Result a
      -> Bool
      -> Restore m a
      -> ParseStream m a
    loop (Partial continue) isFresh rollback s = Effect do
      lr <- next s
      pure $ case lr of
        Right (t, s') ->
          loop (continue t) False (rollback . yield t) s' -- feed and remember
        Left () ->
          if isFresh
            then Return (Return ()) -- good work
            else loop (continue "") False rollback s -- make it fail

    --
    loop Fail {} _ rollback s = Return (rollback s)
    loop (Done rest x) _ _ s =
      yield x do
        start
          do
            if T.null rest
              then Nothing
              else Just rest
          do s
