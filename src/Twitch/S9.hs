{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module Twitch.S9 where 
import Protolude (foldM, (<&>))
import Streaming
import Streaming.Internal
import qualified Streaming.Prelude as S
import Test.Hspec (describe, hspec, it, shouldBe)
import Test.QuickCheck (Gen, choose)

-- a rose tree
data Tree a = Node a [Tree a] deriving (Eq, Show)

-- left euler tour (a.k.a bone XML with no text)
data V a = U | D a deriving (Eq, Show)

-- useful for test (and for solution :-))
reverseNode :: Tree a -> Tree a
reverseNode (Node x xs) = Node x $ reverse xs

-- we don't have Done because we yield
streamTrees :: Functor m => Stream (Of (V a)) m r -> Stream (Of (Tree a)) m (Either r (Stream (Of (V a)) m r))
streamTrees = go Nothing -- Boot
  where
    go _ (Return r) = Return $ Left r
    go mt (Effect m) = Effect $ go mt <$> m
    -- entering new context
    go mt (Step (D x :> rest)) = go
      do Just (Node x [], maybe [] (uncurry (:)) mt)
      do rest
    -- closing a context
    go mt (Step (U :> rest)) = case mt of
      -- closing the zero context , should we repush U?
      Nothing -> Return $ Right $ Step $ U :> rest
      -- stream a tree and attack the rest as fresh
      Just (x, []) -> Step $ reverseNode x :> go Nothing rest
      -- closing a inner context, push the baby in
      Just (x, Node y xs : ys) -> go
        do Just (Node y $ reverseNode x : xs, ys)
        do rest

type MaybeStem c m a r = forall f. Stream f m (Either r (c, Stream (Of (V a)) m r))

-- break when it's time to collect trees, 'c' is *valid* for the next segment of trees
type Selector c m a r = c -> Stream (Of (V a)) m r -> MaybeStem c m a r

-- a flower, c is the stem  up to the node context, node content is streamed
data Flower c m a r = Flower c (Stream (Of (Tree a)) m r) deriving (Functor)

streamAllTrees
  :: forall m c a r.
  Monad m
  => c -- selector state
  -> Selector c m a r -- stream breaker for next stem
  -> Stream (Of (V a)) m r -- stream of tags
  -> Stream (Flower c m a) m r -- stream of flowers
streamAllTrees selectorState selector stream =
  selector selectorState stream
    >>= \case
      Right (c, s) ->
        Step $
          Flower c $
            streamTrees s <&> \case
              Left x -> Return x
              Right stream -> streamAllTrees c selector stream
      Left r -> Return r

data DumbContext a = DumbContext
  { dcDepth :: Int
  , dcContent :: [a]
  }
  deriving (Show, Eq)

popContext :: DumbContext a -> DumbContext a
popContext (DumbContext n (_ : xs)) = DumbContext (pred n) xs

pushContext :: a -> DumbContext a -> DumbContext a
pushContext x (DumbContext n xs) = DumbContext (succ n) (x : xs)

emptyDumbContext :: DumbContext a
emptyDumbContext = DumbContext 0 []

depth :: (Show a, Monad m) => Int -> Selector (DumbContext a) m a r
depth n = go
  where
    go (DumbContext 0 []) (Return r) = Return $ Left r
    go l (Effect m) = Effect $ go l <$> m
    go l (Step (U :> s)) = go (popContext l) s
    go l s@(Step (D x :> s'))
      | dcDepth l < n = go (pushContext x l) s'
      | otherwise = Return $ Right (l, s)

flowers :: (Show a, Monad m) => Int -> Stream (Of (V a)) m r -> Stream (Flower (DumbContext a) m a) m r
flowers n = streamAllTrees emptyDumbContext $ depth n

runFlower :: Monad m => Flower c m a r -> m (Of (F c a) r)
runFlower (Flower n s) = first (F n) <$> S.toList s

listOfFlowers :: Monad m => Stream (Flower c m a) m r -> m (Of [F c a] r)
listOfFlowers = S.toList . S.mapped runFlower

testFlowers :: Show a => Int -> [V a] -> Of [F (DumbContext a) a] ()
testFlowers n xs = runIdentity $ listOfFlowers $ flowers n $ S.each xs

data F c a = F c [Tree a] deriving (Show, Eq)

----------------- proof or die  ---------------------

-- generate a tree node
node
  :: Int -- depth shrinker
  -> Int -- node name
  -> Gen (Tree Int, Int) -- the node and the last name used
node l m = do
  n <- choose (0, l)
  (m', fs) <- foldM
    do
      \(m, rs) _ -> do
        (r, m') <- node (l - 1) m
        pure (m', r : rs)
    do (succ m, [])
    do [1 .. n]
  pure (reverseNode $ Node m fs, m')

main :: IO ()
main = hspec do
  describe "flowers" do
    it "gets no flowers from emptyness" $
      shouldBe
        do testFlowers @Int 0 []
        do [] :> ()
    it "gets a flower" $
      shouldBe
        do testFlowers @Int 0 [D 1, U]
        do [F emptyDumbContext [Node 1 []]] :> ()
    it "gets a bigger flower" $
      shouldBe
        do testFlowers @Int 0 [D 1, D 2, U, U]
        do [F emptyDumbContext [Node 1 [Node 2 []]]] :> ()
    it "gets a stemmed flower" $
      shouldBe
        do testFlowers @Int 1 [D 1, D 2, U, U]
        do [F (DumbContext 1 [1]) [Node 2 []]] :> ()
    it "gets a bigger stemmed flower" $
      shouldBe
        do testFlowers @Int 1 [D 1, D 2, U, D 3, U, U]
        do [F (DumbContext 1 [1]) [Node 2 [], Node 3 []]] :> ()
    it "gets 2 stemmed flowers" $
      shouldBe
        do testFlowers @Int 1 [D 1, D 2, U, D 3, U, U, D 4, D 5, U, U]
        do [F (DumbContext 1 [1]) [Node 2 [], Node 3 []], F (DumbContext 1 [4]) [Node 5 []]] :> ()
