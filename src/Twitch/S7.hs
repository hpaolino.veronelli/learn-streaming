{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Twitch.S7 where

import Control.Monad.Managed (MonadManaged)
import Data.Attoparsec.Text (
  parseOnly,
  sepBy1,
  string,
  takeWhile1,
 )
import Data.Sequence (Seq (..))
import qualified Data.Sequence as Q
import qualified Data.Text as T
import Protolude
import Streaming (Of (..), Stream)
import Streaming.Internal (Stream (Effect, Return, Step))
import Streaming.Learn (streamFile, streamLoop, yield)
import Streaming.Parser (tags)
import qualified Streaming.Prelude as S
import Test.Hspec (describe, hspec, it, shouldBe)
import Text.Taggy.Types (
  Attribute,
  Tag (TagClose, TagOpen, TagText),
 )
import Prelude (tail)

data Context = Context
  { contextName :: Text -- tagname
  , contextAttributes :: [Attribute] -- attributes
  }
  deriving (Eq, Show)

data Node = Leaf Context Text | Node Context [Node]
  deriving (Show, Eq)

type Forest = [Node]

data Partial = Partial [Context] Text 



data Branch = Branch Position (Seq Context) Text
  deriving (Show, Eq)

-- mkNode ::
--   Functor m =>
--   Stream (Of (Branch, Maybe  (Int, Node -> a))) m r ->
--   Stream (Of a) m r
-- mkNode = go Nothing
--  where
--   go _ (Return r) = Return r
--   go mn (Effect m) = Effect $ go mn <$> m
--   go _ (Step (Left (node, n, f) :> rest)) = go (Just (n, f)) rest
--   go Nothing (Step (_ :> rest)) = go Nothing rest

-- go (Just (n,f))

newtype Validate = Validate (Context -> Maybe (Bool, Validate))

matchAll :: Validate
matchAll = Validate $ const $ Just (True, matchAll)

matchNone :: Validate
matchNone = Validate $ const Nothing

newtype XPath = XPath {xpathValidators :: Seq Validate}

type Position = Int

branchHeight :: Branch -> Position
branchHeight (Branch position seq _) = position + length seq

showXPath :: XPath -> Text
showXPath (XPath s) = "XPath " <> show (length s)

appendContext :: Branch -> Context -> Branch
appendContext (Branch p cs t) c = Branch p (cs :|> c) t

setText :: Text -> Branch -> Branch
setText t (Branch p cs _) = Branch p cs t

judgeBranch :: XPath -> Branch -> (XPath, Bool)
judgeBranch xp@(XPath q) (Branch p xs _)
  | p >= length q = (xp, False)
  | otherwise = go True (toList xs) $ XPath $ Q.take (p + 1) q -- truncate the validators after 'p' one
 where
  go :: Bool -> [Context] -> XPath -> (XPath, Bool)
  go j [] xp = (xp, j)
  go _ (x : xs) xp@(XPath vs@(_ :|> Validate v)) = case v x of
    Nothing -> (xp, False)
    Just (j, v') -> go j xs $ XPath $ vs :|> v'

testJudgeBranch ::
  Validate -> -- validator for position 0
  [Branch] -> -- sequence of branches to apply
  [Bool] -- judgnments of each of them
testJudgeBranch e = snd . mapAccumL judgeBranch (XPath $ e :<| Empty)

-------------------  simple path selection ---------------------------------

type Cond = Text -> Bool

data Glob
  = UpTo Cond -- glob everything up to
  | Segment Cond
  | Jolly

readGlob :: [Text] -> [Glob]
readGlob [""] = []
readGlob [] = []
readGlob ("" : t : xs) = UpTo (parseSegment t) : readGlob xs
readGlob ("*" : xs) = Jolly : readGlob xs
readGlob (t : xs) = Segment (parseSegment t) : readGlob xs

parseSegment :: Text -> Cond
parseSegment t =
  let r = parseOnly
        do
          ors <- sepBy1 (takeWhile1 (/= '|')) (string "|")
          pure (`elem` ors)
        do t
   in case r of
        Left e -> panic $ show e
        Right r -> r

-- readGlob _ = panic "read a blog instead"

readQuery :: Text -> Query
readQuery t = Query do
  case T.head t of
    '/' -> tail $ readGlob $ T.splitOn "/" $ "_" <> t
    _ -> readGlob $ T.splitOn "/" $ "/" <> t

newtype Query = Query [Glob]

matchGlob :: Query -> Validate
matchGlob (Query []) = matchAll
matchGlob (Query (x : xs)) = fix $ \e -> Validate \(Context tn _) ->
  let step = (null xs, matchGlob $ Query xs)
   in case x of
        Segment t -> if t tn then Just step else Nothing
        Jolly -> Just step
        UpTo t -> Just if t tn then step else (False, e)

main :: IO ()
main = hspec $ do
  -- describe "readQuery" do
  --   it "can parse a segment" $
  --     shouldBe
  --       do readQuery "/segment"
  --       do Query [Segment "segment"]
  --   it "can parse a upto segment" $
  --     shouldBe
  --       do readQuery "segment"
  --       do Query [UpTo "segment"]
  --   it "can parse a random one" $
  --     shouldBe
  --       do readQuery "segment//ciao/mamma/*///piero"
  --       do Query [UpTo "segment", UpTo "ciao", Segment "mamma", Jolly, UpTo "", Segment "piero"]
  describe "matchGlob" do
    it "can match all" $
      shouldBe
        do
          testJudgeBranch
            (matchGlob $ readQuery "")
            []
        do []
    it "can match a segment" $
      shouldBe
        do
          testJudgeBranch
            (matchGlob $ readQuery "/segment")
            [Branch 0 [Context "segment" []] ""]
        do [True]
    it "can match a segment" $
      shouldBe
        do
          testJudgeBranch
            (matchGlob $ readQuery "segment")
            [Branch 0 [Context "segment" []] ""]
        do [True]
    it "can match a segment root" $
      shouldBe
        do
          testJudgeBranch
            (matchGlob $ readQuery "segment")
            [ Branch 0 [Context "segment" [], Context "else" []] ""
            ]
        do [True]
    it "can match a segment root on 2 nodes" $
      shouldBe
        do
          testJudgeBranch
            (matchGlob $ readQuery "segment")
            [ Branch 0 [Context "segment" [], Context "else" []] ""
            , Branch 1 [Context "more else" []] ""
            ]
        do [True, True]
    it "can match a segment root on 4 nodes, with failing " $
      shouldBe
        do
          testJudgeBranch
            (matchGlob $ readQuery "/segment")
            [ Branch 0 [Context "segment" [], Context "else" []] ""
            , Branch 1 [Context "more else" []] ""
            , Branch 0 [Context "no segment" []] ""
            , Branch 0 [Context "segment" [], Context "more else" []] ""
            ]
        do [True, True, False, True]

-------------------- tags to branches -------------------

-- unroll self closing tags, use Monad Streaming I.E.
-- notice S.for is stateless , we cannot use it for branches
normalizeTags :: Monad m => Stream (Of Tag) m r -> Stream (Of Tag) m r
normalizeTags s = S.for s \case
  TagOpen name attributes True -> do
    S.yield $ TagOpen name attributes False
    S.yield $ TagClose name
  x -> S.yield x

branches :: Functor m => Stream (Of Tag) m r -> Stream (Of Branch) m (r, Position)
branches = go 0 Nothing
 where
  go h Nothing (Return r) = Return (r, h)
  go h xs (Effect m) = Effect $ go h xs <$> m
  go h mb (Step (tag :> stream)) = case tag of
    TagOpen name attributes False ->
      maybe
        do Branch h Empty ""
        do identity
        do mb
        & \b -> go (succ h) (Just $ appendContext b $ Context name attributes) stream
    TagClose _name -> go (pred h) Nothing stream & maybe identity yield mb
    TagText t -> go h (setText (T.strip t) <$> mb) stream
    _ -> go h mb stream

xpathTaint :: Functor m => Validate -> Stream (Of Branch) m r -> Stream (Of (Bool, Branch)) m r
xpathTaint = go . XPath . pure
 where
  go _ (Return r) = Return r
  go xpath (Effect m) = Effect $ go xpath <$> m
  go xpath (Step (x :> rest)) =
    let (xpath', j) = judgeBranch xpath x
     in yield (j, x) $ go xpath' rest

mappendBranch :: Branch -> Branch -> Branch
mappendBranch (Branch p1 s1 t) (Branch p0 s0 _) =
  Branch (min p0 p1) (Q.take (p1 - p0) s0 <> s1) t

collapseFalse ::
  Functor m =>
  Stream (Of (Bool, Branch)) m r ->
  Stream (Of Branch) m (r, Position)
collapseFalse = go 0 Nothing
 where
  go h _ (Return r) = Return (r, h)
  go h b (Effect m) = Effect $ go h b <$> m
  go h mb (Step ((False, x) :> rest)) = go
    do h
    do mappendBranch x <$> mb <|> Just x
    do rest
  go _ mb (Step ((True, x) :> rest)) = yield
    do maybe x (mappendBranch x) mb
    do go (branchHeight x) Nothing rest

fromXML :: MonadManaged m => Text -> FilePath -> Stream (Of Branch) m ((Stream (Of Text) m Int, Position), Position)
fromXML q =
  collapseFalse'
    . xpathTaint (matchGlob $ readQuery q)
    . branches
    . normalizeTags
    . tags
    . streamFile

collapseFalse' ::
  Functor m =>
  Stream (Of (Bool, Branch)) m r ->
  Stream (Of Branch) m (r, Position)
collapseFalse' = streamLoop step return (0, Nothing)
 where
  return (p, _) r = (r, p)
  step (h, mb) ((False, x) :> rest) go = go
    do (h,) $ mappendBranch x <$> mb <|> Just x
    do rest
  step (_, mb) ((True, x) :> rest) go = yield
    do maybe x (mappendBranch x) mb
    do go (branchHeight x, Nothing) rest
