{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Twitch.S5 where

import Protolude
import Streaming
import Streaming.Internal (Stream (..))
import Streaming.Learn
import Streaming.Parser
import Text.Taggy.Types
import Text.Taggy (tag)
import Control.Monad.Managed

data Context = Context
  { contextName :: Text
  , attributes :: [Attribute]
  }
  deriving (Show, Eq)

type Quality = [Context]

data Qualification
  = Change Quality
  | Singleton Quality
  deriving (Show, Eq)

data Qualified
  = Qualification Qualification
  | Content Text
  deriving (Show, Eq)

qualifieds
  :: Functor m
  => Stream (Of Tag) m r
  -> Stream (Of Qualified) m (r, Quality)
qualifieds = loop []
  where
    loop q (Return r) = Return (r, q)
    loop q (Effect m) = Effect $ loop q <$> m
    loop q (Step (tag :> stream)) = case tag of
      TagOpen tn attr True ->
        yield
          (Qualification $ Singleton $ Context tn attr : q)
          $ loop q stream
      TagOpen tn attr False ->
        let q' = Context tn attr : q
         in yield (Qualification $ Change q') $ loop q' stream
      TagText t -> yield (Content t) $ loop q stream
      TagClose tn -> case q of
        (c : q') ->
          if tn /= contextName c
            then panic "unaligned closed tag"
            else yield (Qualification $ Change q') $ loop q' stream
      _ -> loop q stream

fromXML :: MonadManaged m => FilePath -> Stream (Of Qualified) m (Stream (Of Text) m  Int, Quality)
fromXML = qualifieds . streamParser (tag True) . streamFile 