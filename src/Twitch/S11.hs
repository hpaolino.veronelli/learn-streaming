{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Twitch.S11 where

-- https://hackage.haskell.org/package/streaming

-- https://hackage.haskell.org/package/taggy-0.2.1

import Protolude hiding (Selector)
import Streaming
import Streaming.Internal
import qualified Text.Parsec as P
import qualified Text.Parsec.Pos as P
import Text.Taggy

type Tokens m a r = Stream (Of a) m r

instance Monad m => P.Stream (Tokens m a r) m a where
  uncons (Return _) = pure Nothing
  uncons (Effect m) = m >>= P.uncons
  uncons (Step (x :> rest)) = pure (Just (x, rest))

type Parser r (m :: * -> *) a b = P.ParsecT (Tokens m a r) () m b

consumeParser :: Monad m => Parser r m a b -> Tokens m a r -> m (Maybe (b, Maybe (Tokens m a r)))
consumeParser p s = do
  r <- P.runParsecT p $ P.State s (P.initialPos "test") ()
  result <- case r of
    P.Empty r' -> fmap (False,) <$> r'
    P.Consumed r' -> fmap (True,) <$> r'
  pure $ case result of
    P.Ok (c, x) (P.State s' _ ()) _e -> Just (x, if c then Just s' else Nothing)
    P.Error _e -> Nothing

------- against Tags --------------------------------

type TagParser r m a = Parser r m Tag a

data Tree = Tree Text Text [Attribute] [Tree]

tagText :: Monad m => TagParser r m Text
tagText = do
  TagText t <- P.anyToken
  pure t

skipText :: Monad m => TagParser r m ()
skipText = P.skipMany tagText

aLeaf :: Monad m => TagParser r m Tree
aLeaf = do
  TagOpen n attrs True <- P.anyToken
  pure $ Tree n "" attrs []

aTag :: Monad m => TagParser r m Tree
aTag = do
  skipText
  TagOpen n attrs True <- P.anyToken
  v <- childrenOrText
  let tag = (\(t, xs) -> Tree n t attrs xs) $ case v of
        Left xs -> ("", xs)
        Right t -> (t, [])
  tagClose
  pure tag

aTree :: Monad m => TagParser r m Tree
aTree = P.try aLeaf <|> aTag

childrenOrText :: Monad m => TagParser r m (Either [Tree] Text)
childrenOrText = (Left <$> P.try children) <|> (Right <$> tagText)

children :: Monad m => TagParser r m [Tree]
children = do
  skipText
  P.many1 $ do
    t <- aTree
    skipText
    pure t

tagClose :: Monad m => TagParser r m ()
tagClose = do
  TagClose _ <- P.anyToken
  pure ()
