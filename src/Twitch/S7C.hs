{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Twitch.S7C where

--(foldl', identity)

import Control.Monad.Managed
import qualified Data.Sequence as Seq
import Protolude hiding (FilePath)
import Streaming
import Streaming.Learn
import Streaming.Internal
import qualified Streaming.Prelude as S
import Text.Pretty.Simple
import Text.Taggy
import Turtle (FilePath, encodeString)
import Prelude (String)
import qualified Data.Text as T
import Streaming.Parser

data Context = Context
  { contextName :: Text -- tagname
  , contextAttributes :: [Attribute] -- attributes
  }
  deriving (Eq, Show)

type Position = Int

data Branch = Branch Position (Seq.Seq Context) Text
  deriving (Show, Eq)

data Tree = Node Context [Tree] | Leaf Context Text
  deriving (Show, Eq)

type Forest = [Tree]

reverseTree :: Tree -> Tree
reverseTree (Node x r) = Node x (reverseTree <$> reverse r)
reverseTree (Leaf x y) = Leaf x y

-- build a path from a list of nested nodes
mkPath :: [Context] -> Text -> Tree
mkPath [x] y = Leaf x y
mkPath (x : xs) y = Node x [mkPath xs y]

pathFrom :: Int -> [Context] -> Text -> Tree -> Tree
pathFrom 1 cs y (Node x ts) = Node x $ mkPath cs y : ts
pathFrom n cs y (Node x (t : ts)) = Node x $ pathFrom (pred n) cs y t : ts

data Fork = Fork
  { level :: Int --
  , path :: [Context] -- context
  , tip :: Text
  }
  deriving (Show)

reverseForest :: Forest -> Forest
reverseForest = fmap reverseTree . reverse

data Collect
  = Start Int Branch
  | Collect Branch
  deriving (Show)

data Stemmed f a = Stemmed [Context] (f a) deriving (Functor, Show)

newtype StemmedStream m b a = StemmedStream (Stemmed (Stream (Of b) m) a) deriving (Functor)

mapStemmedStream :: (Stream (Of a) m r -> Stream (Of b) m r) -> StemmedStream m a r -> StemmedStream m b r
mapStemmedStream f (StemmedStream (Stemmed cs x)) = StemmedStream $ Stemmed cs $ f x

mkFork :: Position -> Branch -> ([Context], Collect -> Fork)
mkFork n (Branch _i xs _t) = (toList stem, forker)
  where
    stem = Seq.take n xs
    forker (Collect (Branch i xs t)) = Fork (i - n) (toList xs) t

fromBranches
  :: forall m r.
  Monad m
  => Stream (Of Collect) m r
  -> Stream (StemmedStream m Fork) m r
fromBranches = loop
  where
    loop :: Functor m => Stream (Of Collect) m r -> Stream (StemmedStream m Fork) m r
    loop stream = case stream of
      Return r -> Return r
      Effect m -> Effect (fmap loop m)
      Step (Start n b :> rest) ->
        let (context, forker) = mkFork n b
         in Step $
              StemmedStream $
                Stemmed context $
                  S.map forker $
                    fmap loop $ S.break isStart $ yield (Collect b) rest

isStart :: Collect -> Bool
isStart (Start _ _) = True
isStart _ = False

fromForks :: Functor m => Stream (Of Fork) m r -> Stream (Of Tree) m r
fromForks = go Nothing
  where
    go mtree (Return r) = maybe identity yield mtree $ Return r
    go tree (Effect m) = Effect $ go tree <$> m
    go mtree (Step (fork :> rest)) = case fork of
      Fork 0 cs t -> maybe identity yield mtree $ go (Just $ mkPath cs t) rest
      Fork l cs t -> go (pathFrom l cs t <$> mtree) rest

fromStemmed :: Monad m => Stream (StemmedStream m Fork) m r -> Stream (Stemmed (Of Forest)) m r
fromStemmed = S.mapped (\(StemmedStream x) -> foldTrees x) . S.maps (mapStemmedStream fromForks)

foldTrees :: Monad m => Stemmed (Stream (Of Tree) m) x -> m (Stemmed (Of Forest) x)
foldTrees (Stemmed cs trees) = Stemmed cs <$> S.toList trees

streamFlowers :: Monad m => Stream (Of Collect) m r -> Stream (Stemmed (Of Forest)) m r
streamFlowers = fromStemmed . fromBranches

normalizeTags :: Monad m => Stream (Of Tag) m r -> Stream (Of Tag) m r
normalizeTags s = S.for s \case
  TagOpen name attributes True -> do
    S.yield $ TagOpen name attributes False
    S.yield $ TagClose name
  x -> S.yield x

appendContext :: Branch -> Context -> Branch
appendContext (Branch p cs t) c = Branch p (cs Seq.:|> c) t

setText :: Text -> Branch -> Branch
setText t (Branch p cs _) = Branch p cs t

branches :: Functor m => Stream (Of Tag) m r -> Stream (Of Branch) m (r, Position)
branches = go 0 Nothing
  where
    go h Nothing (Return r) = Return (r, h)
    go h xs (Effect m) = Effect $ go h xs <$> m
    go h mb (Step (tag :> stream)) = case tag of
      TagOpen name attributes False ->
        maybe
          do Branch h Seq.Empty ""
          do identity
          do mb
          & \b -> go (succ h) (Just $ appendContext b $ Context name attributes) stream
      TagClose _name -> go (pred h) Nothing stream & maybe identity yield mb
      TagText t -> go h (setText (T.strip t) <$> mb) stream
      _ -> go h mb stream

fromXML
  :: MonadManaged m
  => String
  -> Stream (Stemmed (Of Forest)) m (Stream (Of Text) m Int, Position)
fromXML =
  fromStemmed
    . fromBranches
    . S.mapM (\x -> pPrint x $> x)
    . selectCities
    . branches
    . normalizeTags
    . tags
    . streamFile

selectCities :: Functor m => Stream (Of Branch) m r -> Stream (Of Collect) m r
selectCities = go Nothing
  where
    go _ (Return r) = Return r
    go w (Effect m) = Effect $ go w <$> m
    go Nothing (Step (b@(Branch p s _) :> rest)) = case Seq.findIndexL (\(Context name _) -> name == "city") s of
      Just n -> yield (Start n b) $ go (Just (n + p)) rest
      Nothing -> go Nothing rest
    go (Just n) (Step (b@(Branch p _ _) :> rest))
      | p <= n = go Nothing rest
      | otherwise = yield (Collect b) $ go (Just n) rest

mainDriver :: FilePath -> Int -> Managed ()
mainDriver src n = do
  (n S.:> _) <-
    S.length $
      S.take n $
        S.mapped printStem $ fromXML (encodeString src)
  liftIO $ print n

printStem :: MonadIO m => Stemmed (Of Forest) x -> m (Of () x)
printStem (Stemmed cs (forest :> rest)) = pPrint (cs, forest) >> pure (() :> rest)

{-
main = hspec do
  describe "mkForest" do
    it "maps null to null" do
      shouldBe
        do fromForks @Int []
        do []
    it "maps one change rooted at 0 to one tree" do
      shouldBe
        do fromForks @Int [Fork 0 [1 .. 3]]
        do [Node 1 [Node 2 [Node 3 []]]]
    it "apply a change to a part" do
      shouldBe
        do fromForks @Int [Fork 0 [1, 1], Fork 1 [2]]
        do [Node 1 [Node 1 [], Node 2 []]]
    it "apply 2 changes to a part" do
      shouldBe
        do
          fromForks @Int
            [ Fork 0 [1, 1]
            , Fork 1 [2]
            , Fork 1 [3, 4]
            ]
        do
          [ Node
              1
              [ Node 1 []
              , Node 2 []
              , Node
                  3
                  [ Node 4 []
                  ]
              ]
            ]
    it "create 2 trees if 2 0 changes appear" do
      shouldBe
        do
          fromForks @Int
            [ Fork 0 [1, 1]
            , Fork 1 [2]
            , Fork 0 [3, 4]
            ]
        do
          [ Node
              1
              [ Node 1 []
              , Node 2 []
              ]
            , Node
                3
                [ Node 4 []
                ]
            ]
    it "create a binary tree" do
      shouldBe
        do
          fromForks @Int
            [ Fork 0 [0, 1, 1]
            , Fork 2 [2]
            , Fork 1 [2, 1]
            , Fork 2 [2]
            ]
        do
          [ Node
              0
              [ Node
                  1
                  [ Node 1 []
                  , Node 2 []
                  ]
              , Node
                  2
                  [ Node 1 []
                  , Node 2 []
                  ]
              ]
            ]
 -}
