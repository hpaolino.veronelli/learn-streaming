{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Twitch.S5' where

import qualified Data.Text as T
import Protolude hiding (Down)
import Streaming
import Streaming.Internal (Stream (..))
import Streaming.Learn
import Streaming.Parser
import Text.Taggy (tag)
import Text.Taggy.Types
import Control.Monad.Managed

data Context = Context
  { contextName :: Text
  , attributes :: [Attribute]
  }
  deriving (Show, Eq)

type Quality = [Context]

data Qualified
  = Down Quality -- depth change
  | Up
  | Content Text -- tag without children
  deriving (Show, Eq)

qualifieds
  :: forall m r.
  Functor m
  => Stream (Of Tag) m r
  -> Stream (Of Qualified) m (r, Quality)
qualifieds = loop [] Nothing
  where
    loop :: Quality -> Maybe Text -> Stream (Of Tag) m r -> Stream (Of Qualified) m (r, Quality)
    loop q _ (Return r) = Return (r, q)
    loop q t (Effect m) = Effect $ loop q t <$> m
    loop q mt (Step (tag :> stream)) = case tag of
      TagOpen tn attr True ->
        yield (Down $ Context tn attr : q) $
          yield Up $ loop q Nothing stream
      TagOpen tn attr False ->
        let q' = Context tn attr : q
         in yield (Down q') $ loop q' (Just "") stream
      TagText t' -> loop q ((<> t') <$> mt) stream
      TagClose tn -> case q of
        [] -> panic "who is in the house"
        (c : q') ->
          if tn /= contextName c
            then panic "unaligned closed tag"
            else
              loop q' Nothing stream & yield Up & case mt of
                Just t -> yield (Content $ T.strip t)
                Nothing -> identity
      _ -> loop q mt stream

data Node = Node Quality Text deriving (Show,  Eq)

nodes
  :: forall m r.
  Functor m
  => Stream (Of Qualified) m r
  -> Stream (Of Node) m r
nodes = loop Nothing
  where
    loop _ (Return r) = Return r
    loop mt (Effect m) = Effect $ loop mt <$> m
    loop mt (Step (q :> stream)) = case q of
      Up -> loop Nothing stream & maybe identity yield mt
      Content t -> loop
        do (\(Node x _) -> Node x t) <$> mt
        do stream
      Down q -> loop (Just $ Node q "") stream  

fromXML :: MonadManaged m => FilePath -> Stream (Of Node) m (Stream (Of Text) m Int, Quality)
fromXML = nodes . qualifieds . streamParser (tag True) . streamFile
