{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE TupleSections #-}

newtype Identity' a = Identity' a

data Maybe' a = Nothing' | Just' a

data List a = Cons a (List a) | Nil -- instead of []

newtype Fn' a b = Fn' (a -> b) -- instead of (a -> b)

data Bin a = Leaf a | Bin (Bin a) (Bin a) deriving (Functor)

instance Applicative Bin where
  pure = Leaf
  Leaf f <*> bin = f <$> bin
  Bin l r <*> bin = Bin (l <*> bin) (r <*> bin)

instance Monad Bin where
  Leaf x >>= f = f x
  Bin l r >>= f = Bin (l >>= f) (r >>= f)

data Rose a = Rose [Rose a] | LeafR a deriving (Functor)

instance Applicative Rose where
  pure = LeafR
  LeafR f <*> rose = f <$> rose
  Rose fs <*> rose = Rose $ (<*> rose) <$> fs

instance Monad Rose where
  LeafR x >>= f = f x
  Rose xs >>= f = Rose $ (>>= f) <$> xs

data RoseL a = RoseL a [RoseL a] deriving (Functor)

instance Applicative RoseL where
  pure x = RoseL x []
  RoseL f fs <*> RoseL x xs = RoseL (f x) $ ((f <$>) <$> xs) <> ((<*> pure x) <$> fs)

instance Monad RoseL where
  RoseL x xs >>= f =
    let RoseL y ys = f x
     in RoseL y $ ((>>= f) <$> xs) <> ys

newtype Reader r a = Reader {runReader :: r -> a} deriving (Functor)

instance Applicative (Reader r) where
  pure = Reader . const
  Reader f <*> Reader x = Reader $ f <*> x

instance Monad (Reader r) where
  Reader r >>= f = Reader $ \x ->
    let Reader z = f $ r x
     in z x

newtype State s a = State {runState :: s -> (a, s)} deriving (Functor)

instance Applicative (State s) where
  pure x = State (x,)
  State sf <*> State sx = State $ \s ->
    let (f, s') = sf s
        (x, s'') = sx s'
     in (f x, s'')

instance Monad (State s) where
  State sx >>= f = State $ \s ->
    let (x, s') = sx s
        State sf = f x
     in sf s'

newtype Writer w a = Writer {runWriter :: (a, w)} deriving Functor 

instance Monoid w => Applicative (Writer w) where 
    pure = Writer . (, mempty)
    Writer (f,w) <*> Writer (x,w') = Writer (f x, w <> w') 
    
newtype Cont r a = Cont {runCont :: (a -> r) -> r}

instance Functor (Cont r) where 
    fmap f (Cont x) = Cont $ \g -> x $ g . f 

