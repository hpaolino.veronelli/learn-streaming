{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Test.S4Spec where

import Data.Attoparsec.Text
import qualified Data.Text as T
import Protolude
import Streaming
import Streaming.Internal (Stream (..))
import Streaming.Parser
import Streaming.Prelude (next)
import Test.Hspec
import Text.Taggy (tag)
import Text.Taggy.Types
import Twitch.S4 as S4

steps :: Monad m => Stream (Of a) m b -> m ([a], b)
steps s = do
  x <- next s
  case x of
    Left r -> pure ([], r)
    Right (x, s') -> first (x :) <$> steps s'

testStreamParse :: Monad m => Parser a -> [Text] -> m ([a], [Text])
testStreamParse p ts = do
  (as', s') <- steps do
    S4.streamParser p $ each ts
  (ts', ()) <- steps s'
  pure (as', ts')

spec :: Spec
spec = do
  describe "streaming a name" do
    it "works with aligned parser" do
      shouldReturn
        do
          testStreamParse
            do string "ape"
            do ["ape", "ape", "ape"]
        do (replicate 3 "ape", [])
    it "works with unaligned bigger parser" do
      shouldReturn
        do
          testStreamParse
            do string "ape"
            do ["ap", "ea", "pe", "ap", "e"]
        do (replicate 3 "ape", [])
    it "works with unaligned smaller parser" do
      shouldReturn
        do
          testStreamParse
            do string "ape"
            do ["apea", "peap", "e"]
        do (replicate 3 "ape", [])
    it "returns unparsed text with aligned parser" do
      shouldReturn
        do
          testStreamParse
            do string "ape"
            do ["ape", "ape", "ape", "aaa", "ppp", "eee"]
        do (replicate 3 "ape", ["aaa", "ppp", "eee"])
    it "returns unparsed text with bigger parser" do
      shouldReturn
        do
          testStreamParse
            do string "ape"
            do ["ap", "ea", "pe", "ap", "ea", "aa", "pp", "pe", "ee"]
        do (replicate 3 "ape", ["a", "aa", "pp", "pe", "ee"])
    it "returns unparsed text with smaller parser" do
      shouldReturn
        do
          testStreamParse
            do string "ape"
            do ["apea", "peap", "eaaa", "pppe", "ee"]
        do (replicate 3 "ape", ["aaa", "pppe", "ee"])
