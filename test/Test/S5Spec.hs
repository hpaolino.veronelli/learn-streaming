{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.S5Spec where

import Data.Attoparsec.Text
import Protolude
import Streaming.Learn
import Streaming.Parser as S
import Test.Hspec
import Text.Taggy (tag)
import Text.Taggy.Types
import Twitch.S5

testStreamQualifieds :: Monad m => [Text] -> m ([Qualified], [Text], Quality)
testStreamQualifieds ts = do
  (as', (s', q)) <- steps do
    qualifieds $ S.streamParser (tag True) $ each ts
  (ts', ()) <- steps s'
  pure (as', ts', q)

specQualifieds :: Spec
specQualifieds = do
  describe "streaming qualifieds" do
    it "works with no qualifieds" do
      shouldReturn
        do
          testStreamQualifieds
            do []
        do ([], [], [])
    it "works with one self closing tag" do
      shouldReturn
        do
          testStreamQualifieds
            do ["<a>"]
        do ([Qualification $ Change [Context "a" []]], [], [Context "a" []])
    it "works with one open tag" do
      shouldReturn
        do
          testStreamQualifieds
            do ["<a/>"]
        do
          (,,)
            [ Qualification $ Singleton [Context "a" []]
            ]
            []
            []
    it "works with one open and closed tag" do
      shouldReturn
        do
          testStreamQualifieds
            do ["<a><", "/a>"]
        do
          (,,)
            [ Qualification $ Change [Context "a" []]
            , Qualification $ Change []
            ]
            []
            []
    it "works with text enclosed in one open and closed tag" do
      shouldReturn
        do
          testStreamQualifieds
            do ["<a>hel", "lo<", "/a>"]
        do
          (,,)
            [ Qualification $ Change [Context "a" []]
            , Content "hello"
            , Qualification $ Change []
            ]
            []
            []

testStreamParse :: Monad m => Parser a -> [Text] -> m ([a], [Text])
testStreamParse p ts = do
  (as', s') <- steps do
    S.streamParser p $ each ts
  (ts', ()) <- steps s'
  pure (as', ts')

spec :: Spec
spec = do
  specTag
  specQualifieds

specTag :: Spec
specTag = do
  describe "streaming tags" do
    it "works with no tags" do
      shouldReturn
        do
          testStreamParse
            (tag True)
            do []
        do ([], [])
    it "works with one tag" do
      shouldReturn
        do
          testStreamParse
            (tag True)
            do ["hello"]
        do ([TagText "hello"], [])
    it "works with 2 tags" do
      shouldReturn
        do
          testStreamParse
            (tag True)
            do ["hello<", "a>rest"]
        do ([TagText "hello", TagOpen "a" [] False, TagText "rest"], [])
    it "works with 2 tags and a failure" do
      shouldReturn
        do
          testStreamParse
            (tag True)
            do ["hello<", "a><rest"]
        do ([TagText "hello", TagOpen "a" [] False], ["<rest"])
    it "catches close tags" do
      shouldReturn
        do
          testStreamParse
            (tag True)
            do ["hello<", "a><", "/a><r", "est"]
        do ([TagText "hello", TagOpen "a" [] False, TagClose "a"], ["<r", "est"])
    it "catches enclosed text" do
      shouldReturn
        do
          testStreamParse
            (tag True)
            do ["hello<", "a>enclosed <", "/a><r", "est"]
        do
          (,)
            do
              [ TagText "hello"
                , TagOpen "a" [] False
                , TagText "enclosed "
                , TagClose "a"
                ]
            do ["<r", "est"]
    it "catches selfclosing tags" do
      shouldReturn
        do
          testStreamParse
            (tag True)
            do ["hello<", "a/><", "/a><r", "est"]
        do
          (,)
            do
              [ TagText "hello"
                , TagOpen "a" [] True
                , TagClose "a"
                ]
            do ["<r", "est"]
