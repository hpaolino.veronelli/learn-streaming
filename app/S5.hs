{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}


import Protolude hiding (FilePath)
import qualified Twitch.S5 as S5
import qualified Twitch.S5' as S5'
import Streaming.Learn
import qualified Streaming.Prelude as S
import Text.Pretty.Simple
import Turtle
import Control.Monad.Managed

parser :: Parser (FilePath, Int)
parser =
  (,) <$> argPath "src" "The source file"
    <*> argInt "out" "number of streamed outputs"

main :: IO ()
main = do
  (src, n) <- options "XML filter" parser
  void $ runManaged $ 
    S.effects $
      S.mapM (\x -> pPrint x $> x) $
        S.take n $ S5'.fromXML (encodeString src)
