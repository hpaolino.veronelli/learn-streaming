{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

import Control.Monad.Managed
import Protolude hiding (FilePath)
import qualified Streaming.Prelude as S
import Text.Pretty.Simple
import Turtle hiding (printf)
import qualified Twitch.S12 as S12
import Text.Printf
import Data.String (String)

parser :: Parser (FilePath,  Int)
parser =
  (,) <$> argPath "src" "The source file"
    <*> argInt "out" "number of streamed outputs"

main :: IO ()
main = runManaged do
  (src, n) <- options "XML filter" parser
  (n S.:> _) <-
    S.length $
      S.mapM (\x -> pPrint x $> x) $
          S.take n $ S12.fromXMLF (encodeString src)
  putStrLn @ String $  printf "processed %d elements" n