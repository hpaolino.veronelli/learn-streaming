{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

import Control.Monad.Managed
import Protolude hiding (FilePath)
import qualified Streaming.Prelude as S
import Text.Pretty.Simple
import Turtle hiding (printf)
import qualified Twitch.S7 as S7
import qualified Twitch.S7C as S7C
import Text.Printf
import Data.String (String)

parser :: Parser (FilePath, Int)
parser =
  (,) <$> argPath "src" "The source file"
    -- <*> argText "xpath" "the xpath pattern"
    <*> argInt "out" "number of streamed outputs"

main :: IO ()
main = runManaged do
  (src, n) <- options "XML filter" parser
  S7C.mainDriver src n 


